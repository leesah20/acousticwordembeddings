__author__ = "Lisa van Staden"

import math

import torch
from torch import optim, nn

from data_loading.SpeakerDataLoader import SpeakerDataLoader
from modules.linear_modules import FramePositionPredictor


class FutureFrameReconstructorTrainer:

    def __init__(self, language, input_dim):
        self.model = None

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.language = language
        self.input_dim = input_dim
        self.train_data = None
        self.val_data = None

    def build(self):
        self.model = FramePositionPredictor(50, self.input_dim * 20)

    def load_data(self):
        data_loader = SpeakerDataLoader(dataset_type="validation",
                                        language=self.language,
                                        batch_size=-1,
                                        include_speaker_ids=True,
                                        num_buckets=1, max_seq_len=20,
                                        dframe=self.input_dim)
        data = None
        for batch in data_loader:
            x = batch.X.reshape(len(data_loader), 2, -1)
            x_swapped = x.flip(1)
            data = torch.cat((x.reshape(len(data_loader), -2), x_swapped.reshape(len(data_loader), -2)), 0)
        labels = torch.zeros(len(data_loader) * 2)
        labels[len(data_loader):] = 1 # 0 for correct order, 1 for incorrect order
        idxs = torch.randperm(len(data))
        data = data[idxs]  # shuffle order
        labels = labels[idxs]


        self.train_data = data[:int(0.8 * len(data))]
        self.train_labels = labels[:int(0.8 * len(data))].type(torch.LongTensor)
        self.val_data = data[int(0.8 * len(data)):]
        self.val_labels = labels[int(0.8 * len(data)):].type(torch.LongTensor)


    def train(self, verbose=True, num_epochs=1000, lr=0.01, batch_size=50):

        best_acc = 0
        optimizer = torch.optim.Adam(self.model.parameters(), lr=lr)
        criterion = nn.BCEWithLogitsLoss()

        for e in range(num_epochs):
            self.model.train()
            running_loss = 0
            length = 0
            for segment_batch, label_batch in zip(list(torch.split(self.train_data, batch_size)),
                                            list(torch.split(self.train_data, batch_size))):

                outputs = self.model(segment_batch.to(self.device))
                optimizer.zero_grad()
                loss = criterion(outputs, label_batch.to(self.device))
                loss.backward()
                optimizer.step()

                running_loss += loss.item()
                length += 1

            accuracy = self.evaluate()

            if accuracy > best_acc:
                best_acc = accuracy
                best_model = torch.save(self.model.state_dict(), "frame_speaker_predictor")

            if verbose and e % 10 == 0:
                print("Epoch: ", e, running_loss / length, "Accuracy: ", accuracy)

        print("Best accuracy: ", best_acc)
        if best_model is not None:
            self.model = best_model

    def evaluate(self):
        acc_sum = 0
        for i, segment in enumerate(self.val_data):
            correct = 0
            total = 0
            self.model.eval()
            with torch.no_grad():
                outputs = self.model(segment)
                l = self.val_labels[i].to(self.device)
                _, predicted = torch.max(outputs, 0)
                total += 1
                correct += (predicted == l).sum().item()

            acc_sum += 100 * correct / total

        return acc_sum / len(self.val_data)