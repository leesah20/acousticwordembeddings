import json

from modules import XVectorModel
from training import helpers
from training.APCTrainer import APCTrainer
from training.LengthPredictorTrainer import LengthPredictorTrainer
from training.helpers import custom_mse

__author__ = "Lisa van Staden"

import sys
import torch
from torch import optim
from torch import nn
import numpy as np

from modules.Encoder import Encoder
from modules.AutoEncoder import AutoEncoder
from modules.Decoder import Decoder
from modules.SeqToSeq import SeqToSeq
from data_loading.SpeakerDataLoader import SpeakerDataLoader
from training.SpeakerPredictorTrainer import SpeakerPredictorTrainer


class AETrainer:
    """ Class responsible for building and training an Auto Encoder model given the model attributes"""
    dev_data_loader: SpeakerDataLoader
    test_data_loader: SpeakerDataLoader
    train_data_loader: SpeakerDataLoader
    model: SeqToSeq

    def __init__(self, config, save_path="ae.pt", language="english", config_key="ae",
                 speaker_adversarial=False, speaker_cond=False, gender_adversarial=False,
                 gender_cond=False, with_apc=False, xvector_path=None):
        """
        :param config: dictionary with model configurations
        :param save_path: path to save trained model to
        """
        self.speaker_adversarial = speaker_adversarial
        self.speaker_cond = speaker_cond
        self.gender_adversarial = gender_adversarial
        self.gender_cond = gender_cond
        self.with_apc = with_apc
        self.xvector_path = xvector_path
        if self.with_apc:
            self.apc_trainer = None
        if self.speaker_adversarial:
            self.speaker_predictor_trainer = None
        if self.gender_adversarial:
            self.gender_predictor_trainer = None
        self.model = None
        self.optimizer = None
        self.config = config
        self.save_path = save_path
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        self.language = language

        print(self.device)

        # load datasets

        self.train_data_loader = None
        self.test_data_loader = None
        self.dev_data_loader = None
        self.config_key = config_key

        self._set_data_loaders()

    def build(self):
        """ Builds the model with given attributes or default values"""

        # attributes
        input_dim = self.config[self.config_key]["input_dim"] or 13
        enc_hidden_dim = self.config[self.config_key]["enc_hidden_dim"] or 400
        dec_hidden_dim = self.config[self.config_key]["dec_hidden_dim"] or 400
        n_layers = self.config[self.config_key].get("n_layers") or 3
        n_cond_layers = self.config[self.config_key].get("n_cond_layers") or 3
        n_z = self.config[self.config_key]["n_z"] or 130
        rnn_mode = self.config[self.config_key]["rnn_mode"] or "RNN"
        ae_enc_layers = self.config[self.config_key]["ae_enc_layers"] or []
        ae_dec_layers = self.config[self.config_key]["ae_dec_layers"] or [400]
        dropout = self.config[self.config_key].get("dropout") or 0
        speaker_embedding_dim = 0 if not (self.speaker_cond or self.gender_cond) \
            else self.config[self.config_key].get("speaker_embedding_dim")
        embedding_dim = (self.speaker_cond + self.gender_cond) * speaker_embedding_dim

        num_speakers = self.train_data_loader.get_num_speakers()
        num_genders = self.train_data_loader.get_num_genders()

        # model
        encoder = Encoder(input_dim, enc_hidden_dim, n_layers, rnn_mode, dropout)
        ae = AutoEncoder(enc_hidden_dim, ae_enc_layers, n_z, ae_dec_layers)
        decoder = Decoder(ae.get_output_dim() + embedding_dim, dec_hidden_dim, input_dim, n_layers,
                          rnn_mode, dropout, num_cond_layers=n_cond_layers, num_speakers=num_speakers,
                          num_genders=num_genders, speaker_embedding_dim=speaker_embedding_dim)

        self.model = SeqToSeq(encoder, decoder, ae, num_speakers if self.speaker_cond else 0,
                              num_genders if self.gender_cond else 0, speaker_embedding_dim).to(self.device)

        learning_rate = self.config[self.config_key]["learning_rate"]
        self.optimizer = optim.Adam(self.model.parameters(), lr=learning_rate)

        if self.speaker_adversarial:
            self.speaker_predictor_trainer = SpeakerPredictorTrainer(self.config, num_speakers)
            self.speaker_predictor_trainer.build()
        if self.gender_adversarial:
            self.gender_predictor_trainer = SpeakerPredictorTrainer(self.config, num_genders)
            self.gender_predictor_trainer.build()

        if self.with_apc:
            with open('/home/config/cpc_config.json') as apc_config_file:
                apc_config = json.load(apc_config_file)
                self.apc_trainer = APCTrainer(config=apc_config, checkpoint_path="/home/saved_models/apc",
                                              language="english_cpc",
                                              include_aux_loss=True)
            self.apc_trainer.build()

        if self.speaker_cond and self.xvector_path:
            xvectors = np.load(self.xvector_path)
            xvec_emb_weights = torch.zeros((num_speakers, speaker_embedding_dim), device=self.device)

            for speaker in xvectors:
                speaker_idx = self.train_data_loader.speaker_id_to_idx(speaker)
                xvec_emb_weights[speaker_idx] = torch.as_tensor(xvectors[speaker], device=self.device)

            self.model.load_embeddings_from_pretraied(xvec_emb_weights)

        if self.gender_cond and self.xvector_path:
            xvectors = np.load(self.xvector_path)

            xvec_emb_weights = torch.zeros((num_genders, speaker_embedding_dim), device=self.device)

            for gender in xvectors:
                gender_idx = 0 if gender == 'f' else 1
                xvec_emb_weights[gender_idx] = torch.as_tensor(xvectors[gender], device=self.device)

            self.model.load_embeddings_from_pretraied(xvec_emb_weights, gender=True)

    def load_saved_model(self):
        print("loading saved model")
        self.model.load_state_dict(torch.load(self.save_path))

    def train(self, loss_fn=custom_mse, verbose=True, num_epochs=-1, lr=0.001, mode='None', loss_limit=-1.0, save_if_ap=0):
        n_epochs = self.config[self.config_key]["n_epochs"] if num_epochs == -1 else num_epochs

        # batch_size = self.config[self.config_key]]["batch_size"] or 100
        # n_buckets = self.config[self.config_key]]["n_buckets"] or 1
        if self.speaker_adversarial or self.gender_adversarial:
            sp_wait_for_num_epochs = self.config[self.config_key].get("sp_wait") or 50
            # sp_adversarial_rate = self.config[self.config_key].get("sp_adversarial_rate") or 2


        best_ap = save_if_ap
        best_epoch = 0
        for epoch in range(n_epochs):

            if self.dev_data_loader is not None:
                eval_results = self.evaluate(normalise=False,
                                             verbose=epoch % 1 == 0)
                if eval_results["ap"] > best_ap:

                    torch.save(self.model.state_dict(), self.save_path)
                    best_ap = eval_results["ap"]
                    best_epoch = epoch
                elif epoch > 100 and eval_results['ap'] / best_ap < 0.5:
                    break

            epoch_loss, zs, speakers, genders, indices = \
                self._train_step(self.optimizer, self.train_data_loader, loss_fn, epoch)

            if self.speaker_adversarial and epoch >= sp_wait_for_num_epochs:
                zs = torch.cat(zs)
                speakers = torch.cat(speakers)
                indices = sum(indices, [])
                self.speaker_predictor_trainer.update_data(zs, speakers, indices)
                if epoch == sp_wait_for_num_epochs:
                    self.speaker_predictor_trainer.train()
                else:
                    self.speaker_predictor_trainer.train(epochs=1)

            if self.gender_adversarial and epoch >= sp_wait_for_num_epochs:
                if not self.speaker_adversarial:
                    zs = torch.cat(zs)
                    indices = sum(indices, [])
                genders = torch.cat(genders)
                self.gender_predictor_trainer.update_data(zs, genders, indices)

                if epoch == sp_wait_for_num_epochs:
                    self.gender_predictor_trainer.train()
                else:
                    self.gender_predictor_trainer.train(epochs=1)

            if self.with_apc:
                self.apc_trainer.train(num_epochs=1)
            self.optimizer.step()

            if verbose and epoch % 1 == 0:
                print("Epoch {0}:\t Loss: {1}\t".format(epoch, epoch_loss))

            if self.dev_data_loader is not None:
                print("Best AP:", best_ap, "at", best_epoch)

            if epoch_loss < loss_limit:
                print("Training loss limit reached")
                break

        if self.dev_data_loader is None:
            torch.save(self.model.state_dict(), self.save_path)

        self.load_saved_model()
        return best_ap

    def _train_step(self, optimizer, data_loader: SpeakerDataLoader, loss_fn, epoch):
        self.model.train()
        epoch_loss = 0
        length = 0
        zs = []
        speakers = []
        genders = []
        indices = []
        sp_criterion = nn.CrossEntropyLoss()

        if self.speaker_adversarial or self.gender_adversarial:
            sp_wait_for_num_epochs = self.config[self.config_key].get("sp_wait") or 50

        for i, batch in enumerate(data_loader):
            optimizer.zero_grad()
            x = batch.X.to(self.device)

            enc_c, z, dec_pred, mask, outputs_enc = self.model(x, x_seq_lengths=batch.X_lengths,
                                                               speaker_idxs=batch.speaker_X_idx.to(self.device)
                                                               if self.speaker_cond else None,
                                                               gender_idxs=batch.gender_X_idx.to(self.device)
                                                               if self.gender_cond else None)

            if self.speaker_adversarial or self.gender_adversarial:
                zs.append(z)
                indices.append(batch.indices)

                if self.speaker_adversarial:
                    speakers.append(batch.speaker_X_idx)
                if self.gender_adversarial:
                    genders.append(batch.gender_X_idx)

            y = (dec_pred * torch.unsqueeze(mask, dim=-1)).to(self.device)

            loss = loss_fn(x, y, mask)

            sp_weight = (self.config["ae"].get("sp_loss_weight") or 0.1) * epoch

            if self.speaker_adversarial and epoch > sp_wait_for_num_epochs:
                speaker_ids = batch.speaker_X_idx.long().to(self.device)
                sp_loss = sp_criterion(self.speaker_predictor_trainer.predict_speaker(z), speaker_ids)
                loss -= sp_weight * sp_loss

            if self.gender_adversarial and epoch > sp_wait_for_num_epochs:
                gender_ids = batch.gender_X_idx.long().to(self.device)
                sp_loss = sp_criterion(self.gender_predictor_trainer.predict_speaker(z), gender_ids)
                loss -= sp_weight * sp_loss

            loss.backward()
            nn.utils.clip_grad_norm_(self.model.parameters(), 10)

            optimizer.step()

            epoch_loss += loss.item()
            length += 1
        return epoch_loss / length, zs, speakers, genders, indices

    def evaluate(self, data="validate", normalise=True, verbose=True):
        results = {}

        self.model.eval()

        if data == "validate":
            data_loader = self.dev_data_loader
        elif data == "test":
            data_loader = self.test_data_loader
        else:
            print("Error: invalid data")
            sys.exit()

        for dp in data_loader:
            x = dp.X.to(self.device)
            labels = dp.labels

            with torch.no_grad():
                enc_c, z, dec_pred, mask, outputs_enc = self.model(x, dp.X_lengths)

                z = z.cpu().numpy()
            results["ap"], results["prb"] = helpers.calculate_ap_and_prb(z, labels)

            if normalise:
                z_norm = z - np.mean(z, axis=0)
                z_norm = z_norm / np.std(z_norm, axis=0)

                results["ap_norm"], results["prb_norm"] = helpers.calculate_ap_and_prb(z_norm, labels)

        if verbose:
            print("Average Precision:", results["ap"], "Precision-recall Breakeven:", results["prb"])
            if normalise:
                print("Normalised Average Precision:", results["ap_norm"],
                      "Normalised Precision-recall Breakeven:", results["prb_norm"])

        return results

    def evaluate_speaker_predictability(self, data="validate", gender=False, linear=False):
        self.model.eval()
        data_loader = self.dev_data_loader if data == "validate" else self.test_data_loader

        if not gender:
            speaker_predictor_trainer = SpeakerPredictorTrainer(config=self.config,
                                                                num_speakers=data_loader.get_num_speakers(),
                                                                linear=linear)
        else:
            speaker_predictor_trainer = SpeakerPredictorTrainer(config=self.config,
                                                                num_speakers=data_loader.get_num_genders(),
                                                                linear=linear)
        speaker_predictor_trainer.build()

        for batch in data_loader:
            x = batch.X.to(self.device)
            if not gender:
                speaker_labels = batch.speaker_X_idx
            else:
                speaker_labels = batch.gender_X_idx

            with torch.no_grad():
                enc_c, z, dec_pred, mask, outputs_enc = self.model(x, batch.X_lengths)

            speaker_predictor_trainer.load_data(z, speaker_labels, batch.indices)

        speaker_predictor_trainer.train(verbose=False)
        acc = speaker_predictor_trainer.evaluate()
        return acc

    def evaluate_length_predictability(self, verbose=True, data="validate"):
        self.model.eval()
        data_loader = self.dev_data_loader if data == "validate" else self.test_data_loader
        length_predictor_trainer = LengthPredictorTrainer(config=self.config)
        length_predictor_trainer.build()
        for batch in data_loader:
            x = batch.X.to(self.device)
            lengths = batch.X_lengths
            with torch.no_grad():
                enc_c, z, dec_pred, mask, outputs_enc = self.model(x, lengths)
            length_predictor_trainer.load_data(z, lengths, batch.indices)
            break

        length_predictor_trainer.train(verbose=False)
        return length_predictor_trainer.evaluate()

    def _set_data_loaders(self):

        batch_size = self.config[self.config_key]["batch_size"] or 100
        input_dim = self.config[self.config_key]["input_dim"]
        num_buckets = self.config[self.config_key].get("n_buckets") or 3
        num_speaker_lim = self.config[self.config_key].get("max_num_speakers") or -1
        max_pairs = self.config[self.config_key].get("max_num_pairs") or -1

        adaptive_sampler = self.config_key == "incr_ae"

        self.train_data_loader = SpeakerDataLoader(dataset_type="training", language=self.language,
                                                   batch_size=batch_size,
                                                   include_speaker_ids=(self.speaker_adversarial or self.speaker_cond
                                                                        or adaptive_sampler),
                                                   include_gender_ids=(self.gender_adversarial or self.gender_cond),
                                                   num_buckets=num_buckets, adaptive_sampler=adaptive_sampler,
                                                   num_speaker_lim=num_speaker_lim,
                                                   max_pairs=max_pairs, dframe=input_dim)

        self.test_data_loader = SpeakerDataLoader(dataset_type="test", language=self.language, batch_size=0,
                                                  include_speaker_ids=True, include_gender_ids=True, dframe=input_dim)

        if not self.language.startswith("xitsonga"):
            self.dev_data_loader = SpeakerDataLoader(dataset_type="validation", language=self.language, batch_size=0,
                                                     include_speaker_ids=True, include_gender_ids=True,
                                                     dframe=input_dim)

    def embeddings_to_file(self, path, normalise=True, data='validate'):

        self.model.eval()

        if data == "validate":
            data_loader = self.dev_data_loader
        elif data == "test":
            data_loader = self.test_data_loader
        else:
            print("Error: invalid data")
            sys.exit()

        for dp in data_loader:
            x = dp.X.to(self.device)
            keys = dp.utt_key
            print(x.size())
            with torch.no_grad():
                enc_c, z, dec_pred, mask, outputs_enc = self.model(x, dp.X_lengths)

                z = z.cpu().numpy()
                embed_dict = {}

                if normalise:
                    z = z - np.mean(z, axis=0)
                    z = z / np.std(z, axis=0)

                for i in range(len(keys)):
                    embed_dict[keys[i]] = z[i]

                np.savez_compressed(path, **embed_dict)

    def extract_speaker_embeddings(self, path):
        embedding_dict = {}
        if self.speaker_cond or self.gender_cond:
            if self.speaker_cond:
                num_embeddings = self.train_data_loader.get_num_speakers()
                embeddings = self.model.speaker_embedding.weight.data.cpu().numpy()
                for k in range(num_embeddings):
                    embedding_dict[self.train_data_loader.speaker_idx_to_id(k)] = embeddings[k]
            else:
                embeddings = self.model.gender_embedding.weight.data.cpu().numpy()
                embedding_dict["f"] = embeddings[0]
                embedding_dict["m"] = embeddings[1]
            np.savez_compressed(path, **embedding_dict)

        else:
            print("No speaker or gender embeddings")


if __name__ == '__main__':
    with open('/home/config/herman_model_config.json') as config_file:
        config = json.load(config_file)

        for i in range(1, 4):
            trainer = AETrainer(config, f"/home/saved_models/combined/ae_speak_speak_{i}.pt", language="english",
                                speaker_cond=True)
            trainer.build()
            trainer.load_saved_model()
