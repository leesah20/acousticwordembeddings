__author__ = "Lisa van Staden"

import sys

from data_loading.SpeakerDataLoader import SpeakerDataLoader
from training.AETrainer import AETrainer
import numpy as np


class IncreasingSpeakerAETrainer(AETrainer):
    def __init__(self, config, save_path="/saved_models/incr_speaker_ae.pt",
                 language="english", config_key="incr_ae", mode="multi", **kwargs):
        """
        :param config: dictionary with model configurations
        :param save_path: path to save trained model to
        """
        super().__init__(config, save_path=save_path, language=language, config_key=config_key, **kwargs)
        self.mode = mode

    def train(self, **kwargs):
        epochs = [100]
        if self.mode == "incr":
            speaker_nums = np.arange(1, self.train_data_loader.get_num_speakers() + 1)
            epochs = [20 for _ in range(self.train_data_loader.get_num_speakers())]
            epochs[0] = 120
            epochs[-1] = 50
        elif self.mode == "single":
            speaker_nums = [1]
        elif self.mode == "multi":
            speaker_nums = [self.train_data_loader.get_num_speakers()]
        elif self.mode == "decr":
            speaker_nums = np.arange(self.train_data_loader.get_num_speakers(), 0, -1)
            epochs = [20 for _ in range(self.train_data_loader.get_num_speakers())]
            epochs[0] = 120
            epochs[-1] = 50
        else:
            print("Invalid mode given")
            sys.exit(0)
        best_ap = 0
        for speaker_num, n_epochs in zip(speaker_nums, epochs):
            print(f'Number of speakers: {speaker_num}')
            self.train_data_loader.set_num_speakers(speaker_num)
            ap = super().train(num_epochs=n_epochs)
            best_ap = max(ap, best_ap)


