__author__ = "Lisa van Staden"

import math

import torch
from torch import optim, nn

from data_loading.SpeakerDataLoader import SpeakerDataLoader
from modules.linear_modules import FrameSpeakerPredictor


class FrameSpeakerPredictorTrainer:

    def __init__(self, language, input_dim, num_speakers):
        self.model = None

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.language = language
        self.input_dim = input_dim
        self.num_speakers = num_speakers
        self.train_data = None
        self.train_speakers = None
        self.val_data = None
        self.val_speakers = None

    def build(self):
        self.model = FrameSpeakerPredictor(self.input_dim * 10, self.num_speakers).to(self.device)

    def load_data(self):
        data_loader = SpeakerDataLoader(dataset_type="validation",
                                        language=self.language,
                                        batch_size=0,
                                        include_speaker_ids=True,
                                        num_buckets=1, max_seq_len=10,
                                        dframe=self.input_dim)
        data = None
        speakers = None
        for batch in data_loader:

            data = batch.X[:, :10, :]
            data = data.reshape(len(data), -1)
            speakers = batch.speaker_X_idx
        self.train_data = data[:int(0.8 * len(data))]
        self.train_speakers = speakers[:int(0.8 * len(data))].type(torch.LongTensor)
        self.val_data = data[int(0.8 * len(data)):]
        self.val_speakers = speakers[int(0.8 * len(data)):].type(torch.LongTensor)

    def train(self, verbose=True, num_epochs=20, lr=0.01, batch_size=50):

        best_acc = 0
        optimizer = torch.optim.Adam(self.model.parameters(), lr=lr, weight_decay=1e-5)
        criterion = nn.CrossEntropyLoss()
        best_epoch = 0

        for e in range(num_epochs):
            self.model.train()
            running_loss = 0
            length = 0

            for segment_batch, label_batch in zip(list(torch.split(self.train_data, batch_size)),
                                            list(torch.split(self.train_speakers, batch_size))):

                outputs = self.model(segment_batch.to(self.device))
                optimizer.zero_grad()
                loss = criterion(outputs, label_batch.to(self.device))
                loss.backward()
                nn.utils.clip_grad_norm_(self.model.parameters(), 1)
                optimizer.step()

                running_loss += loss.item()
                length += 1

            accuracy = self.evaluate()

            if accuracy > best_acc:
                best_acc = accuracy
                best_epoch = e
                best_model = torch.save(self.model.state_dict(), "frame_speaker_predictor")

            if verbose and e % 10 == 0:
                print("Epoch: ", e, running_loss / length, "Accuracy: ", accuracy)

        print("Best accuracy: ", best_acc, "at epoch: ", best_epoch)
        if best_model is not None:
            self.model = best_model

    def evaluate(self):
        acc_sum = 0
        correct = 0
        total = 0
        for i, segment in enumerate(self.val_data):

            self.model.eval()
            with torch.no_grad():
                outputs = self.model(segment.to(self.device))
                s = self.val_speakers[i].to(self.device)
                _, predicted = torch.max(outputs, 0)
                total += 1
                correct += (predicted == s).sum().item()

        acc_sum = 100 * correct / total

        return acc_sum

if __name__ == '__main__':
    trainer = FrameSpeakerPredictorTrainer(language="english", input_dim=13, num_speakers=8)
    trainer.build()
    trainer.load_data()
    trainer.train(verbose=False)