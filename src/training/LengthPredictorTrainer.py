import torch
from torch import nn

from modules.UttLengthPredictor import LengthPredictor
from modules.LinearSpeakerPredictor import LinearSpeakerPredictor

__author__ = "Lisa van Staden"


class LengthPredictorTrainer(object):

    def __init__(self, config):
        self.config = config
        self.model = None
        self.train_set = None
        self.train_labels = None
        self.val_set = None
        self.val_labels = None
        self.train_indices = []
        self.val_indices = []
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    def build(self):
        z_dim = self.config.get("z_dim") or 130
        self.model = LengthPredictor(input_dim=z_dim).to(self.device)

    def train(self, verbose=True, epochs=0):
        best_model = None
        if epochs == 0:
            epochs = self.config["length_predictor"]["n_epochs"]
        lr = self.config["length_predictor"]["learning_rate"]
        batch_size = self.config["length_predictor"].get("batch_size") or 256

        best_r = -1000
        best_epoch = 0
        optimizer = torch.optim.Adam(self.model.parameters(), lr=lr)
        criterion = nn.MSELoss()
        # TODO add scheduler

        for e in range(epochs):
            self.model.train()
            best_model = torch.save(self.model.state_dict(), "length_predictor")
            running_loss = 0
            length = 0
            for z_batch, length_batch in zip(list(torch.split(self.train_set, batch_size)),
                                            list(torch.split(self.train_labels, batch_size))):
                batch = z_batch.detach()
                s = length_batch.to(self.device)
                outputs = self.model(batch).squeeze()
                optimizer.zero_grad()
                loss = criterion(outputs, s)
                loss.backward()
                optimizer.step()

                running_loss += loss.item()
                length += 1

            r_squared = self.evaluate()

            if r_squared > best_r:
                best_r = r_squared
                best_epoch = e
                best_model = torch.save(self.model.state_dict(), "speaker_predictor")

            if verbose and e % 10 == 0:
                print("Epoch: ", e, running_loss / length, "R Squared: ", r_squared)

        print("Best R: ", best_r, "at Epoch:", best_epoch)
        if best_model is not None:
            self.model = best_model

    def evaluate(self):
        self.model.eval()
        with torch.no_grad():
            pred_lengths = self.model(self.val_set).squeeze()
            true_lengths = self.val_labels.to(self.device)
            true_length_mean = torch.ones_like(true_lengths) * torch.mean(true_lengths)
            ss_res = nn.functional.mse_loss(pred_lengths, true_lengths)
            ss_tot = nn.functional.mse_loss(true_lengths, true_length_mean)
            r_squared = 1 - (ss_res / ss_tot)
        return r_squared

    def predict_length(self, z):
        self.model.eval()
        output = self.model(z)
        return output

    def load_data(self, z, length_lables, indices):
        data_length = len(indices)
        self.train_indices = range(int(0.8 * data_length))
        self.val_indices = range(int(0.8 * data_length), data_length)
        self.train_set = z[self.train_indices].detach()
        self.train_labels = length_lables[self.train_indices].detach()
        self.val_set = z[self.val_indices].detach()
        self.val_labels = length_lables[self.val_indices].detach()

