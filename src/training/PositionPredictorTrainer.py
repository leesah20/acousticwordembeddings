__author__ = "Lisa van Staden"

import math

import torch
from torch import optim, nn

from data_loading.SpeakerDataLoader import SpeakerDataLoader
from modules.linear_modules import FramePositionPredictor


class PositionPredictorTrainer:

    def __init__(self, language, input_dim):
        self.model = None

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.language = language
        self.input_dim = input_dim
        self.train_data = None
        self.train_labels = None
        self.val_data = None
        self.val_labels = None

    def build(self):
        self.model = FramePositionPredictor(self.input_dim * 50).to(self.device)

    def load_data(self):
        data_loader = SpeakerDataLoader(dataset_type="validation",
                                        language=self.language,
                                        batch_size=0,
                                        include_speaker_ids=True,
                                        num_buckets=1, max_seq_len=50,
                                        dframe=self.input_dim)
        data = None
        for batch in data_loader:
            x = batch.X
            x = x.reshape(len(x), 2, -1)
            x_swapped = x.flip(1)
            data = torch.cat((x.reshape(len(x), -1), x_swapped.reshape(len(x), -1)), 0)
        labels = torch.zeros(len(data))
        labels[len(x):] = 1 # 0 for correct order, 1 for incorrect order
        idxs = torch.randperm(len(data))
        data = data[idxs]  # shuffle order
        labels = labels[idxs]

        self.train_data = data[:int(0.8 * len(data))]
        self.train_labels = labels[:int(0.8 * len(data))]
        self.val_data = data[int(0.8 * len(data)):]
        self.val_labels = labels[int(0.8 * len(data)):]


    def train(self, verbose=True, num_epochs=50, lr=0.001, batch_size=10):

        best_acc = 0
        optimizer = torch.optim.Adam(self.model.parameters(), lr=lr)
        criterion = nn.BCEWithLogitsLoss()
        best_epoch = 0

        for e in range(num_epochs):
            self.model.train()
            running_loss = 0
            length = 0
            for segment_batch, label_batch in zip(list(torch.split(self.train_data, batch_size)),
                                            list(torch.split(self.train_labels, batch_size))):

                outputs = self.model(segment_batch.to(self.device)).reshape((-1,))
                optimizer.zero_grad()
                loss = criterion(outputs, label_batch.to(self.device))
                loss.backward()
                optimizer.step()

                running_loss += loss.item()
                length += 1

            accuracy = self.evaluate()

            if accuracy > best_acc:
                best_acc = accuracy
                best_epoch = e
                best_model = torch.save(self.model.state_dict(), "frame_speaker_predictor")

            if verbose and e % 10 == 0:
                print("Epoch: ", e, running_loss / length, "Accuracy: ", accuracy)

        print("Best accuracy: ", best_acc, "at epoch: ", best_epoch)
        if best_model is not None:
            self.model = best_model

    def evaluate(self):
        correct = 0
        total = 0
        for i, segment in enumerate(self.val_data):
            self.model.eval()
            with torch.no_grad():
                output = self.model(segment.to(self.device))
                target = self.val_labels[i].to(self.device)
                total += 1
                correct += ((output.item() + target.item()) < 0.5) or ((target.item() * output.item()) >= 0.5)

        acc = 100 * correct / total

        return acc

if __name__ == '__main__':
    trainer = PositionPredictorTrainer(language="english_capc_feats", input_dim=512)
    trainer.build()
    trainer.load_data()
    trainer.train(verbose=False)