__author__ = "Lisa van Staden"

import torch
from torch import optim, nn

from data_loading.SpeakerDataLoader import SpeakerDataLoader
from modules.linear_modules import LogBilinearFramePrediction


class LinearPredictorTrainer:

    def __init__(self, language, input_dim):
        self.model = None

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.language = language
        self.input_dim = input_dim
        self.train_data = None
        self.val_data = None

    def build(self):
        self.model = LogBilinearFramePrediction(50, self.input_dim).to(self.device)

    def load_data(self):
        data_loader = SpeakerDataLoader(dataset_type="validation",
                                        language=self.language,
                                        batch_size=0,
                                        include_speaker_ids=True,
                                        num_buckets=1, max_seq_len=51,
                                        dframe=self.input_dim)
        data = None
        for batch in data_loader:
            data = batch.X

        self.train_data = data[:int(0.8 * len(data))]
        self.val_data = data[int(0.8 * len(data)):]

    def train(self, verbose=True, num_epochs=50, lr=0.001, batch_size=10):

        best_acc = 0
        optimizer = torch.optim.Adam(self.model.parameters(), lr=lr)
        best_epoch = 0

        for e in range(num_epochs):
            self.model.train()
            running_loss = 0
            length = 0
            for segment_batch in list(torch.split(self.train_data, batch_size)):
                loss, _ = self.model(segment_batch.to(self.device))
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                running_loss += loss.item()
                length += 1

            accuracy = self.evaluate()

            if accuracy > best_acc:
                best_acc = accuracy
                best_epoch = e
                best_model = torch.save(self.model.state_dict(), "frame_speaker_predictor")

            if verbose and e % 10 == 0:
                print("Epoch: ", e, running_loss / length, "Accuracy: ", accuracy)

        print("Best accuracy: ", best_acc, "at epoch: ", best_epoch)
        if best_model is not None:
            self.model = best_model

    def evaluate(self):
        self.model.eval()
        with torch.no_grad():
            _, acc = self.model(self.val_data.to(self.device))
        return acc.item()


if __name__ == '__main__':
    trainer = LinearPredictorTrainer(language="english_capc_feats", input_dim=512)
    trainer.build()
    trainer.load_data()
    trainer.train(verbose=False)
