import json

from training import helpers
from training.AETrainer import AETrainer

__author__ = "Lisa van Staden"

import os
import torch
from torch import nn
import numpy as np

from data_loading.SpeakerDataLoader import SpeakerDataLoader


class CAETrainer(AETrainer):
    """ Class responsible for building and training an Auto Encoder model given the model attributes"""

    def __init__(self, config, save_path="/saved_models/cae.pt", pretrained_ae_path="/saved_models/ae.pt",
                 language="english", config_key="cae", speaker_adversarial=False,
                 speaker_cond=False, gender_adversarial=False, gender_cond=False, xvector_path=None):
        """
        :param config: dictionary with model configurations
        :param save_path: path to save trained model to
        """

        self.config_key = config_key

        super().__init__(config, save_path=save_path, language=language, config_key=config_key,
                         speaker_adversarial=speaker_adversarial, speaker_cond=speaker_cond,
                         gender_adversarial=gender_adversarial, gender_cond=gender_cond, xvector_path=xvector_path)

        if os.getcwd() == "/home":
            root = "/home"
        else:
            root = "../.."
        if pretrained_ae_path is not None:
            self.pretrained_ae_path = root + pretrained_ae_path

    def load_pretrained_ae(self):
        """ Builds the model with given attributes or default values"""
        super().build()
        print("Loading weights from", self.pretrained_ae_path)
        self.model.load_state_dict(torch.load(self.pretrained_ae_path))

        if self.speaker_cond and self.xvector_path:

            num_speakers = self.train_data_loader.get_num_speakers()
            speaker_embedding_dim = 0 if not (self.speaker_cond or self.gender_cond) \
                else self.config[self.config_key].get("speaker_embedding_dim")
            xvectors = np.load(self.xvector_path)

            xvec_emb_weights = torch.zeros((num_speakers, speaker_embedding_dim), device=self.device)

            for speaker in xvectors:
                speaker_idx = self.train_data_loader.speaker_id_to_idx(speaker)
                xvec_emb_weights[speaker_idx] = torch.as_tensor(xvectors[speaker], device=self.device)

            self.model.load_embeddings_from_pretraied(xvec_emb_weights)

    def _train_step(self, optimizer, data_loader: SpeakerDataLoader, loss_fn, epoch):
        self.model.train()
        epoch_loss = 0
        length = 0
        zs = []
        indices = []
        speakers = []
        genders = []
        sp_criterion = nn.CrossEntropyLoss()

        if self.speaker_adversarial or self.gender_adversarial:
            sp_wait_for_num_epochs = self.config[self.config_key].get("sp_wait") or 50

        for i, batch in enumerate(data_loader):
            optimizer.zero_grad()

            x = batch.X.to(self.device)
            y = batch.Y.to(self.device)

            enc_c, z, dec_pred, mask, outputs_enc = self.model(x, x_seq_lengths=batch.X_lengths,
                                                               y_seq_lengths=batch.Y_lengths,
                                                               speaker_idxs=batch.speaker_Y_idx.to(self.device)
                                                               if self.speaker_cond else None,
                                                               gender_idxs=batch.gender_Y_idx.to(self.device)
                                                               if self.gender_cond else None)

            if self.speaker_adversarial or self.gender_adversarial:
                zs.append(z)
                indices.append(batch.indices)
                if self.speaker_adversarial:
                    speakers.append(batch.speaker_X_idx)
                if self.gender_adversarial:
                    genders.append(batch.gender_X_idx)

            pred_y = (dec_pred * torch.unsqueeze(mask, dim=-1)).to(self.device)

            loss = loss_fn(pred_y, y, mask)
            sp_loss_weight = (self.config["cae"].get("sp_loss_weight") or 0.1) * epoch

            if self.speaker_adversarial and epoch > sp_wait_for_num_epochs:
                speaker_ids = batch.speaker_X_idx.long().to(self.device)
                sp_loss = sp_criterion(self.speaker_predictor_trainer.predict_speaker(z), speaker_ids)
                loss -= sp_loss_weight * sp_loss

            if self.gender_adversarial and epoch > sp_wait_for_num_epochs:
                gender_ids = batch.gender_X_idx.long().to(self.device)
                sp_loss = sp_criterion(self.gender_predictor_trainer.predict_speaker(z), gender_ids)
                loss -= sp_loss_weight * sp_loss

            loss.backward()
            nn.utils.clip_grad_norm_(self.model.parameters(), 10)

            optimizer.step()

            epoch_loss += loss.item()
            length += 1
        return epoch_loss / length, zs, speakers, genders, indices

    def _set_data_loaders(self):
        print("setting data loaders")
        batch_size = self.config[self.config_key]["batch_size"] or 100
        num_buckets = self.config[self.config_key].get("n_buckets") or 3
        num_speaker_lim = self.config[self.config_key].get("max_num_speakers") or -1
        max_pairs = self.config[self.config_key].get("max_num_pairs") or -1
        input_dim = self.config[self.config_key]["input_dim"]
        adaptive_sampler = self.config_key == "incr_cae"
        print(adaptive_sampler)
        self.train_data_loader = SpeakerDataLoader(dataset_type="training", language=self.language,
                                                   batch_size=batch_size,
                                                   include_speaker_ids=(self.speaker_adversarial or self.speaker_cond or adaptive_sampler),
                                                   include_gender_ids=(self.gender_adversarial or self.gender_cond),
                                                   num_buckets=num_buckets, pairs=True,
                                                   num_speaker_lim=num_speaker_lim, max_pairs=max_pairs,
                                                   dframe=input_dim, adaptive_sampler=adaptive_sampler)

        self.test_data_loader = SpeakerDataLoader(dataset_type="test", language=self.language, batch_size=0,
                                                  include_speaker_ids=True, include_gender_ids=True, dframe=input_dim)

        if not self.language.startswith("xitsonga"):
            self.dev_data_loader = SpeakerDataLoader(dataset_type="validation", language=self.language, batch_size=0,
                                                     include_speaker_ids=True, include_gender_ids=True,
                                                     dframe=input_dim)


if __name__ == '__main__':
    with open('/home/config/herman_model_config.json') as config_file:
        config = json.load(config_file)

        ap_results = []
        sp_results = []
        gp_results = []

        i = 3


        trainer = CAETrainer(config, f"/home/saved_models/cae_apc_{i}.pt",
                                 f"/saved_models/ae_{i}.pt", language="english_cpc_feats")

        trainer.build()
        trainer.load_saved_model()
        print(trainer.evaluate_speaker_predictability(linear=True))
