__author__ = "Lisa van Staden"

import json
import numpy as np
import sys

from training import helpers
from training.CAETrainer import CAETrainer
from training.IncreasingSpeakerAETrainer import IncreasingSpeakerAETrainer


class IncreasingSpeakerCAETrainer(CAETrainer):
    def __init__(self, config, save_path="/saved_models/incr_speaker_cae.pt",
                 language="english", config_key="incr_cae", mode="multi", **kwargs):
        """
        :param config: dictionary with model configurations
        :param save_path: path to save trained model to
        """
        print(language)
        super().__init__(config, save_path=save_path, language=language, config_key=config_key, **kwargs)
        self.mode = mode

    def train(self, **kwargs):
        epochs = [100]
        if self.mode == "incr":
            speaker_nums = np.arange(1, self.train_data_loader.get_num_speakers() + 1)
            epochs = [15 for _ in range(self.train_data_loader.get_num_speakers())]
            epochs[-1] = 20
        elif self.mode == "single":
            speaker_nums = [1]
        elif self.mode == "multi":
            speaker_nums = [self.train_data_loader.get_num_speakers()]
        elif self.mode == "decr":
            speaker_nums = np.arange(self.train_data_loader.get_num_speakers(), 0, -1)
            epochs = [15 for _ in range(self.train_data_loader.get_num_speakers())]
            epochs[-1] = 20
        else:
            print("Invalid mode given")
            sys.exit(0)
        print(epochs)
        print(speaker_nums)
        best_ap = 0
        for speaker_num, n_epochs in zip(speaker_nums, epochs):
            print(f'Number of speakers: {speaker_num} for {n_epochs}')
            self.train_data_loader.set_num_speakers(speaker_num)

            ap = super().train(num_epochs=n_epochs, save_if_ap=best_ap)
            best_ap = max(ap, best_ap)


if __name__ == '__main__':
    with open('/home/config/herman_model_config.json') as config_file:
        config = json.load(config_file)
        '''
        print("MULTI")

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerAETrainer(config, f"/home/saved_models/curriculum/ae_multi_{i}.pt",
                                                 language="hausa", mode="multi")

            trainer.build()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100)) 

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_multi_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_multi_{i}.pt",
                                                  language="hausa",
                                                  mode="multi")

            trainer.build()
            #trainer.load_pretrained_ae()
            trainer.load_saved_model()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        # ------------------------------------------------------------------------- 

        print("SINGLE")

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerAETrainer(config, f"/home/saved_models/curriculum/ae_single_{i}.pt",
                                                 language="hausa", mode="single")

            trainer.build()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_single_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_single_{i}.pt",
                                                  language="hausa",
                                                  mode="single")

            trainer.build()
            trainer.load_pretrained_ae()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        # --------------------------------------------------------------------------

        print("CAE SINGLE")
        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_cae_single_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_single_{i}.pt",
                                                  language="hausa",
                                                  mode="single")

            trainer.build()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        # -------------------------------------------------------------------------- '''

        print("AE SINGLE CAE INCR")
        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_incr_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_single_{i}.pt",
                                                  language="hausa",
                                                  mode="incr")

            trainer.build()
            trainer.load_pretrained_ae()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        # -------------------------------------------------------------------------

        print("AE Incr CAE MULTI")

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerAETrainer(config, f"/home/saved_models/curriculum/ae_incr_{i}.pt",
                                                 language="hausa", mode="incr")

            trainer.build()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_multi_incr_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_incr_{i}.pt",
                                                  language="hausa",
                                                  mode="multi")

            trainer.build()
            trainer.load_pretrained_ae()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        print("CAE Incr")

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_cae_incr_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_incr_{i}.pt",
                                                  language="hausa",
                                                  mode="incr")

            trainer.build()
            # trainer.load_pretrained_ae()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        # -------------------------------------------------------------------------------

        '''print("AE SINGLE CAE INCR")

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_e_incr_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_e_single_{i}.pt",
                                                  language="english",
                                                  mode="incr")

            trainer.build()
            trainer.load_pretrained_ae()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        # -------------------------------------------------------------------------

        print("AE Incr CAE MULTI")

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerAETrainer(config, f"/home/saved_models/curriculum/ae_e_incr_{i}.pt",
                                                 language="english", mode="incr")

            trainer.build()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_e_multi_incr_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_e_incr_{i}.pt",
                                                  language="english",
                                                  mode="multi")

            trainer.build()
            trainer.load_pretrained_ae()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100)) '''

        # -----------------------------------------------------------

        print("AE multi CAE decr")

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_decr_multi_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_multi_{i}.pt",
                                                  language="hausa",
                                                  mode="decr")

            trainer.build()
            trainer.load_pretrained_ae()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        # -----------------------------------------------------------

        print("AE decr CAE single")

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerAETrainer(config, f"/home/saved_models/curriculum/ae_decr_{i}.pt",
                                                 language="hausa", mode="decr")

            trainer.build()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))


        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_single_decr_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_decr_{i}.pt",
                                                  language="hausa",
                                                  mode="single")

            trainer.build()
            trainer.load_pretrained_ae()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        # -----------------------------------------------------------

        print("AE incr CAE incr")

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_incr_incr_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_incr_{i}.pt",
                                                  language="hausa",
                                                  mode="incr")

            trainer.build()
            trainer.load_pretrained_ae()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        # -----------------------------------------------------------

        print("AE decr CAE decr")

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_decr_decr_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_decr_{i}.pt",
                                                  language="hausa",
                                                  mode="decr")

            trainer.build()
            trainer.load_pretrained_ae()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        # -----------------------------------------------------------

        print("AE incr CAE decr")

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_decr_incr_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_incr_{i}.pt",
                                                  language="hausa",
                                                  mode="decr")

            trainer.build()
            trainer.load_pretrained_ae()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

        # -----------------------------------------------------------

        print("AE decr CAE incr")

        ap_results = []
        ap_results_test = []

        for i in range(1, 4):
            trainer = IncreasingSpeakerCAETrainer(config, f"/home/saved_models/curriculum/cae_incr_decr_{i}.pt",
                                                  pretrained_ae_path=f"/saved_models/curriculum/ae_decr_{i}.pt",
                                                  language="hausa",
                                                  mode="decr")

            trainer.build()
            trainer.load_pretrained_ae()
            trainer.train()
            ap_results.append(trainer.evaluate(normalise=True)["ap_norm"])
            ap_results_test.append(trainer.evaluate(data="test", normalise=True)["ap_norm"])

        print("Val: ", helpers.print_string(ap_results, k=100))
        print("Test: ", helpers.print_string(ap_results_test, k=100))

