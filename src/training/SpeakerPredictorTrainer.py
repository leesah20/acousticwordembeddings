import torch
from torch import nn

from modules.SpeakerPredictor import SpeakerPredictor
from modules.LinearSpeakerPredictor import LinearSpeakerPredictor

__author__ = "Lisa van Staden"


class SpeakerPredictorTrainer(object):

    def __init__(self, config, num_speakers, linear=False):
        self.num_speakers = num_speakers
        self.config = config
        self.model = None
        self.train_set = None
        self.train_labels = None
        self.val_set = None
        self.val_labels = None
        self.train_indices = []
        self.val_indices = []
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.linear = linear

    def build(self):
        z_dim = self.config.get("z_dim") or 130
        hidden_dim = self.config.get("hidden_dim") or 400

        if not self.linear:
            self.model = SpeakerPredictor(z_dim, hidden_dim, self.num_speakers).to(self.device)
        else:
            self.model = LinearSpeakerPredictor(z_dim, self.num_speakers).to(self.device)

    def update_data(self, batched_embedding_inputs, speaker_labels, indices):
        if not self.train_indices:
            self.load_data(batched_embedding_inputs,speaker_labels, indices)
            return
        train_indices = [i for i in indices if i in self.train_indices]
        val_indices = [i for i in indices if i in self.val_indices]
        self.train_set = batched_embedding_inputs[train_indices].detach()
        self.train_labels = speaker_labels[train_indices].detach()
        self.val_set = batched_embedding_inputs[val_indices].detach()
        self.val_labels = speaker_labels[val_indices].detach()

    def train(self, verbose=True, epochs=0):
        best_model = None
        if epochs == 0:
            epochs = self.config["speaker_predictor"]["n_epochs"]
        lr = self.config["speaker_predictor"]["learning_rate"]
        batch_size = self.config["speaker_predictor"].get("batch_size") or 256

        best_acc = 0
        optimizer = torch.optim.Adam(self.model.parameters(), lr=lr)
        criterion = nn.CrossEntropyLoss()

        for e in range(epochs):
            self.model.train()
            best_model = torch.save(self.model.state_dict(), "speaker_predictor")
            running_loss = 0
            length = 0
            for z_batch, label_batch in zip(list(torch.split(self.train_set, batch_size)),
                                            list(torch.split(self.train_labels, batch_size))):
                batch = z_batch.detach()
                s = label_batch.type(torch.LongTensor).to(self.device)
                outputs = self.model(batch)
                optimizer.zero_grad()
                loss = criterion(outputs, s)
                loss.backward()
                optimizer.step()

                running_loss += loss.item()
                length += 1

            accuracy = self.evaluate()

            if accuracy > best_acc:
                best_acc = accuracy
                best_model = torch.save(self.model.state_dict(), "speaker_predictor")

            if verbose and e % 10 == 0:
                print("Epoch: ", e, running_loss / length, "Accuracy: ", accuracy)

        print("Best accuracy: ", best_acc)
        if best_model is not None:
            self.model = best_model

    def evaluate(self):
        acc_sum = 0
        for i, z in enumerate(self.val_set):

            correct = 0
            total = 0
            self.model.eval()
            with torch.no_grad():
                outputs = self.model(z)
                s = self.val_labels[i].long().to(self.device)
                _, predicted = torch.max(outputs, 0)
                total += 1
                correct += (predicted == s).sum().item()

            acc_sum += 100 * correct / total

        return acc_sum / len(self.val_set)

    def predict_speaker(self, z):
        self.model.eval()
        output = self.model(z)
        return output

    def load_data(self, z, speaker_labels, indices):
        data_length = len(indices)
        self.train_indices = range(int(0.8 * data_length))
        self.val_indices = range(int(0.8 * data_length), data_length)
        self.update_data(z, speaker_labels, indices)
