__author__ = "Lisa van Staden"

import torch
from torch import nn


class Decoder(nn.Module):

    def __init__(self, input_dim, hidden_dim, output_dim, n_layers=1, mode="RNN_TANH", dropout=0,
                 num_cond_layers=0, num_speakers=0, num_genders=0, speaker_embedding_dim=0):

        super(Decoder, self).__init__()

        self.n_layers = n_layers
        self.n_cond_layers = num_cond_layers
        self.dropout = dropout

        if mode == 'GRU':

            if num_cond_layers <= 1:
                self.rnn = nn.GRU(input_size=input_dim, hidden_size=hidden_dim,
                                  num_layers=n_layers, batch_first=True, dropout=self.dropout)
            else:
                self.rnn = nn.ModuleList([nn.GRU(input_size=input_dim if i == 0 else hidden_dim + speaker_embedding_dim,
                                                 hidden_size=hidden_dim,
                                                 batch_first=True, dropout=self.dropout) for i in
                                          range(num_cond_layers)]
                                         + [nn.GRU(input_size=hidden_dim, hidden_size=hidden_dim,
                                                   batch_first=True, dropout=self.dropout) for _ in
                                            range(n_layers - num_cond_layers)])
        elif mode == 'LSTM':
            self.rnn = nn.LSTM(input_size=input_dim, hidden_size=hidden_dim,
                               num_layers=n_layers, batch_first=True, dropout=self.dropout)
        elif mode == 'RNN_RELU':
            self.rnn = nn.RNN(input_size=input_dim, hidden_size=hidden_dim,
                              num_layers=n_layers, nonlinearity='relu', batch_first=True, dropout=self.dropout)

        else:
            # RNN with tanh activation function
            self.rnn = nn.RNN(input_size=input_dim, hidden_size=hidden_dim,
                              num_layers=n_layers, batch_first=True, dropout=self.dropout)

        self.output_dim = output_dim

        self.out = nn.Linear(hidden_dim, self.output_dim)

        self.hidden_dim = hidden_dim

        if num_speakers > 0 and num_cond_layers > 1:
            print("Adding speaker conditioning for {0} speaker".format(num_speakers))
            self.speaker_embedding = nn.ModuleList([nn.Embedding(num_genders, speaker_embedding_dim)
                                                   for _ in range(num_cond_layers - 1)])

        if num_genders > 0 and num_cond_layers > 1:
            print("Adding gender conditioning for {0} speaker".format(num_genders))
            self.gender_embedding = nn.ModuleList([nn.Embedding(num_genders, speaker_embedding_dim)
                                                   for _ in range(num_cond_layers - 1)])

    def forward(self, src, max_seq_length, speaker_idxs=None, gender_idxs=None):

        if self.n_cond_layers <= 1:
            output, h_n = self.rnn(src)
            h_n_last_layer = h_n[-1]

        else:
            x, _ = self.rnn[0](src)
            for i in range(1, self.n_cond_layers):
                if speaker_idxs is not None:
                    x = torch.cat(
                        (x, self.speaker_embedding[i - 1](speaker_idxs.long()).unsqueeze(1).repeat(1, x.size()[1], 1)), dim=2)

                if gender_idxs is not None:
                    x = torch.cat((x, self.gender_embedding[i - 1](gender_idxs.long()).unsqueeze(1).repeat(1, x.size()[1], 1)),
                                  dim=2)
                x, _ = self.rnn[i](x)

            if self.n_layers - self.n_cond_layers > 0:
                for i in range(self.n_cond_layers, self.n_layers):
                    x, _ = self.rnn[i](x)
            output = x
            h_n_last_layer = output[-1]
        (max_output, _) = torch.max(torch.abs(output), dim=2)

        mask = torch.sign(max_output)

        output = torch.reshape(output, [-1, self.hidden_dim])

        pred = self.out(output)

        pred = torch.reshape(pred, [-1, max_seq_length, self.output_dim])

        return pred, mask, h_n_last_layer

    def load_embeddings(self, embeddings, speaker=True, norm=None):
        if speaker:
            for i in range(self.n_cond_layers - 1):
                self.speaker_embedding[i] = nn.Embedding.from_pretrained(embeddings, max_norm=norm)
