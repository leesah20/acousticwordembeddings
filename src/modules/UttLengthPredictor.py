__author__ = "Lisa van Staden"

from torch import nn


class LengthPredictor(nn.Module):
    def __init__(self, input_dim):
        super(LengthPredictor, self).__init__()
        self.linear = nn.Linear(input_dim, 1)

    def forward(self, z):
        return self.linear(z)
