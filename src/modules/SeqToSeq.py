from modules.AutoEncoder import AutoEncoder
from modules.Decoder import Decoder
from modules.Encoder import Encoder

__author__ = "Lisa van Staden"

from torch import nn
import torch


class SeqToSeq(nn.Module):

    def __init__(self, encoder: Encoder, decoder: Decoder, ae: AutoEncoder, num_speakers=0, num_genders=0,
                 speaker_embedding_dim=0):
        super(SeqToSeq, self).__init__()

        self.encoder = encoder
        self.decoder = decoder
        self.ae = ae

        if num_speakers > 0:
            print("Adding speaker conditioning for {0} speaker".format(num_speakers))
            self.speaker_embedding = nn.Embedding(num_speakers, speaker_embedding_dim)
            self.decoder.load_embeddings(self.speaker_embedding.weight.data)

        if num_genders > 0:
            print("Adding gender conditioning for {0} speaker".format(num_genders))
            self.gender_embedding = nn.Embedding(num_genders, speaker_embedding_dim)

    def forward(self, x, x_seq_lengths, y_seq_lengths=None, speaker_idxs=None, gender_idxs=None):

        if y_seq_lengths is not None:
            max_dec_seq_length = torch.max(y_seq_lengths)
        else:
            max_dec_seq_length = torch.max(x_seq_lengths)

        outputs, enc_c = self.encoder(x, x_seq_lengths)

        z, x = self.ae(enc_c)

        dec_pred = None
        mask = None

        if self.training:

            if speaker_idxs is not None:
                x = torch.cat((x, self.speaker_embedding(speaker_idxs.long())), dim=1)

            if gender_idxs is not None:
                x = torch.cat((x, self.gender_embedding(gender_idxs.long())), dim=1)

            d_latent_layer_output = list(x.size())[-1]

            dec_input = x.repeat((1, 1, int(max_dec_seq_length.item())))

            dec_input = dec_input.reshape([-1, int(max_dec_seq_length.item()), d_latent_layer_output])

            dec_pred, mask, dec_c = self.decoder(dec_input, int(max_dec_seq_length.item()), speaker_idxs=speaker_idxs, gender_idxs=gender_idxs)

        return enc_c, z, dec_pred, mask, outputs

    def load_embeddings_from_pretraied(self, embeddings, gender=False):
        if not gender:
            print("adding speaker conditioning")
            self.speaker_embedding = nn.Embedding.from_pretrained(embeddings, freeze=False, max_norm=1)
        else:
            self.gender_embedding = nn.Embedding.from_pretrained(embeddings, freeze=False, max_norm=1)
        # self.decoder.load_embeddings(self.speaker_embedding)
