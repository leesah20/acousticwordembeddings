__author__ = "Lisa van Staden"

from torch import nn


class Encoder(nn.Module):

    def __init__(self, input_dim, hidden_dim, n_layers=1, mode="RNN_TANH", dropout=0):

        super(Encoder, self).__init__()

        self.n_layers = n_layers

        self.dropout = dropout

        if mode == 'GRU':
            self.rnn = nn.GRU(input_size=input_dim, hidden_size=hidden_dim,
                              num_layers=n_layers, batch_first=True, dropout=self.dropout)
        elif mode == 'LSTM':
            self.rnn = nn.LSTM(input_size=input_dim, hidden_size=hidden_dim,
                               num_layers=n_layers, batch_first=True, dropout=self.dropout)
        elif mode == 'RNN_RELU':
            self.rnn = nn.RNN(input_size=input_dim, hidden_size=hidden_dim,
                              num_layers=n_layers, nonlinearity='relu', batch_first=True, dropout=self.dropout)

        else:
            # RNN with tanh activation function
            self.rnn = nn.RNN(input_size=input_dim, hidden_size=hidden_dim,
                              num_layers=n_layers, batch_first=True, dropout=self.dropout)

    def forward(self, src, seq_lengths):
        src_packed = nn.utils.rnn.pack_padded_sequence(src, seq_lengths, batch_first=True, enforce_sorted=False)
        output, h_n = self.rnn(src_packed)
        output, _ = nn.utils.rnn.pad_packed_sequence(output, batch_first=True)
        h_n_last_layer = h_n[-1]

        return output, h_n_last_layer
