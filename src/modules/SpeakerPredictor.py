from torch import nn

__author__ = "Lisa van Staden"

from torch import nn


class SpeakerPredictor(nn.Module):

    def __init__(self, z_dim, hidden_dim, n_speakers):
        super(SpeakerPredictor, self).__init__()
        print("n_speakers", n_speakers)
        self.ffn1 = nn.Sequential(nn.Linear(z_dim, hidden_dim),
                                  nn.ReLU(),
                                  # nn.Dropout(0.5),
                                  nn.Linear(hidden_dim, hidden_dim),
                                  nn.ReLU(),
                                  nn.Dropout(0.5),
                                  # nn.Linear(hidden_dim, hidden_dim),
                                  # nn.ReLU(),
                                  nn.Linear(hidden_dim, n_speakers),
                                  nn.Softmax(dim=0)
                                  )

    def forward(self, z):
        return self.ffn1(z)
