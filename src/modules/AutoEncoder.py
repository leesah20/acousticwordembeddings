__author__ = "Lisa van Staden"

from collections import OrderedDict
from torch import nn


class AutoEncoder(nn.Module):

    def __init__(self, input_dim, enc_n_hidden, n_z, dec_n_hidden):

        super(AutoEncoder, self).__init__()

        enc_layers = OrderedDict()

        for i, h in enumerate(enc_n_hidden):
            enc_layers["enc_linear_{}".format(i + 1)] = nn.Linear(input_dim, h)
            enc_layers["enc_relu_{}".format(i + 1)] = nn.ReLU()
            input_dim = h

        self.encoder = nn.Sequential(enc_layers)

        self.latent_var = nn.Linear(input_dim, n_z)
        input_dim = n_z

        dec_layers = OrderedDict()

        for i, h in enumerate(dec_n_hidden):
            dec_layers["dec_linear_{}".format(i + 1)] = nn.Linear(input_dim, h)
            if i != len(dec_n_hidden) - 1:
                dec_layers["dec_relu_{}".format(i + 1)] = nn.ReLU()
            input_dim = h

        self.decoder = nn.Sequential(dec_layers)
        self.output_dim = input_dim

    def get_output_dim(self):
        return self.output_dim

    def forward(self, src):
        enc = self.encoder(src)
        z = self.latent_var(enc)
        dec = self.decoder(z)

        return z, dec
