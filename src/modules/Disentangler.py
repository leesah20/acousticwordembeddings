import torch
from torch import nn

from modules.AutoEncoder import AutoEncoder
from modules.Decoder import Decoder
from modules.Encoder import Encoder

__author__ = "Lisa van Staden"


class Disentangler(nn.Module):

    def __init__(self, encoder1: Encoder, encoder2: Encoder, ae1: AutoEncoder, ae2: AutoEncoder ,decoder: Decoder, num_speakers=0, num_genders=0,
                 speaker_embedding_dim=0):
        super(Disentangler, self).__init__()

        self.encoder1 = encoder1
        self.encoder2 = encoder2
        self.ae1 = ae1
        self.ae2 = ae2
        self.decoder = decoder

        if num_speakers > 0:
            print("Adding speaker conditioning for {0} speaker".format(num_speakers))
            self.speaker_embedding = nn.Embedding(num_speakers, speaker_embedding_dim)

        if num_genders > 0:
            print("Adding gender conditioning for {0} speaker".format(num_genders))
            self.gender_embedding = nn.Embedding(num_genders, speaker_embedding_dim)

    def forward(self, src, x_seq_lengths=None, y_seq_lengths=None, speaker_idxs=None, gender_idxs=None):

        if y_seq_lengths is not None:
            max_dec_seq_length = torch.max(y_seq_lengths)
        else:
            max_dec_seq_length = torch.max(x_seq_lengths)

        outputs1, hn1 = self.encoder1(src, x_seq_lengths)
        outputs2, hn2 = self.encoder2(src, x_seq_lengths)

        z1, x1 = self.ae1(hn1)
        z2, x2 = self.ae2(hn2)

        dec_pred = None
        mask = None

        if self.training:

            x = torch.cat((x1, x2), dim=1)

            if speaker_idxs is not None:
                x = torch.cat((x, self.speaker_embedding(speaker_idxs.long())), dim=1)

            if gender_idxs is not None:
                x = torch.cat((x, self.gender_embedding(gender_idxs.long())), dim=1)

            d_latent_layer_output = list(x.size())[-1]

            dec_input = x.repeat((1, 1, int(max_dec_seq_length.item())))

            dec_input = dec_input.reshape([-1, int(max_dec_seq_length.item()), d_latent_layer_output])

            dec_pred, mask, dec_c = self.decoder(dec_input, int(max_dec_seq_length.item()))

        return z1, dec_pred, mask
