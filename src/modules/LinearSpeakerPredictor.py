from torch import nn

__author__ = "Lisa van Staden"

from torch import nn


class LinearSpeakerPredictor(nn.Module):

    def __init__(self, z_dim, n_speakers):
        super(LinearSpeakerPredictor, self).__init__()
        print("n_speakers", n_speakers)
        print("linear speaker classifier")
        self.layer = nn.Linear(z_dim, n_speakers)

    def forward(self, z):
        return self.layer(z)
