__author__ = "Lisa van Staden"

from torch import nn


class Encoder(nn.Module):

    def __init__(self, z_dim):
        super().__init__()
        self.encoder = nn.Sequential(
            nn.Linear(z_dim, z_dim),
            nn.LayerNorm(z_dim),
            nn.ReLU(inplace=True),
            nn.Linear(z_dim, z_dim, bias=False),
            nn.LayerNorm(z_dim),
            nn.ReLU(inplace=True),
            nn.Linear(z_dim, z_dim)
        )

    def forward(self, z):
         return self.encoder(z)


class Predictor(nn.Module):

    def __init__(self, z_dim):
        super().__init__()
        self.W = nn.Linear(z_dim, z_dim)

    def forward(self, ae, z, num_speakers):
        _, z_dim = z.size()

        ae = ae.reshape(num_speakers, -1, z_dim)
        z = z.reshape(num_speakers, -1, z_dim)

        for speaker_idx in range(num_speakers):


