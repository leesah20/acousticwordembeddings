__author__ = "Lisa van Staden"
from torch import nn
import torch
import torch.nn.functional as F
import math


class FramePositionPredictor(nn.Module):

    def __init__(self, input_dim):
        super(FramePositionPredictor, self).__init__()
        self.linear = nn.Linear(input_dim, 1)

    def forward(self, frame):
        return self.linear(frame)


class FrameSpeakerPredictor(nn.Module):

    def __init__(self, input_dim, num_speakers):
        super(FrameSpeakerPredictor, self).__init__()
        self.linear = nn.Linear(input_dim, num_speakers)

    def forward(self, frame):
        return self.linear(frame)


# bad idea, apparently
class CAEReconsructor(nn.Module):
    def __init__(self, input_dim):
        super(CAEReconsructor, self).__init__()
        self.linear = nn.Linear(input_dim, input_dim)

    def forward(self, frame):
        return self.linear(frame)

# maybe bad idea
class FutureFrameReconstructor(nn.Module):
    def __init__(self, input_dim):
        super(FutureFrameReconstructor, self).__init__()
        self.linear = nn.Linear(input_dim, input_dim)

    def forward(self, frame):
        return self.linear(frame)


class LogBilinearFramePrediction(nn.Module):
    def __init__(self, context_size, input_dim):
        super(LogBilinearFramePrediction, self).__init__()
        self.C = nn.ModuleList([nn.Linear(input_dim, input_dim) for _ in range(context_size)])

    def forward(self, frame_seq, num_speakers=1, num_negatives=5):
        [batch_size, seq_len, frame_dim] = frame_seq.size()
        sample_len = seq_len - 1
        utts_per_speaker = batch_size // num_speakers
        q_hat = torch.zeros((batch_size, frame_dim), device=frame_seq.device)
        for i in range(sample_len):
            q_hat += self.C[i](frame_seq[:, i, :])

        # q_hat = q_hat.view(num_speakers, -1, frame_dim)
        # q = frame_seq[:, -1, :].view(num_speakers, -1, frame_dim)
        q = frame_seq[:, -1, :]

        # batch_index = torch.randint(
        #    0, utts_per_speaker,
        #    size=(
        #        utts_per_speaker,
        #        num_negatives
        #    ),
        #    device=frame_seq.device
        # )
        # batch_index = batch_index.view(
        #     1, utts_per_speaker, num_negatives, 1
        # )

        batch_index = torch.randint(0, batch_size, size=(batch_size, num_negatives, 1), device=frame_seq.device)

        speaker_index = torch.arange(num_speakers, device=frame_seq.device)
        speaker_index = speaker_index.view(-1, 1, 1, 1)

        seq_index = torch.randint(0, seq_len, size=(batch_size, num_negatives, 1), device=frame_seq.device)

        negative_frames = frame_seq[batch_index, seq_index, :].squeeze(2)

        all_frames = torch.cat((q.unsqueeze(1), negative_frames), dim=1)
        score = torch.sum(all_frames * q_hat.unsqueeze(1) / math.sqrt(frame_dim), dim=-1)
        score = score.view(batch_size, num_negatives + 1)
        labels = torch.zeros(batch_size, dtype=int).to(frame_seq.device)
        loss = F.cross_entropy(score, labels)
        acc = torch.mean((score.argmax(dim=1) == labels).float())
        return loss, acc
