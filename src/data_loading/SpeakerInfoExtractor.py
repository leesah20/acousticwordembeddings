__author__ = "Lisa van Staden"

import torch

from data_loading.SpeakerDataLoader import SpeakerDataLoader
from data_loading.SpeakerDataset import SpeakerDataset


class SpeakerInfoExtractor(object):
    """ Object used to extract and plot information about speakers"""

    def __init__(self, language='english', dataset_type='training'):
        self.data_loader = SpeakerDataLoader(language=language, dataset_type=dataset_type, batch_size=1, pairs=True,
                                            include_speaker_ids=True, include_gender_ids=True, for_analysis=True)

        self.speakers = []
        self.genders = []
        self.speech_segment_count = torch.zeros(self.data_loader.get_num_speakers())
        self.speaker_pair_segments_count = torch.zeros((self.data_loader.get_num_speakers(),
                                                        self.data_loader.get_num_speakers()))
        self.speaker_to_idx = {}
        self.extract_speaker_info()

    def get_speaker_ids(self):
        return self.speakers

    def get_gender(self, speaker_id):
        return self.genders[self.speaker_to_idx[speaker_id]]

    def get_speech_segment_count(self, speaker_id):
        pass

    def get_pair_count(self, speaker_X_id, speaker_Y_id=None):
        if speaker_Y_id is None:
            speaker_Y_id = speaker_X_id
        return self.speaker_pair_segments_count[speaker_X_id][speaker_Y_id]

    def extract_speaker_info(self):

        # data_loader[0] = batch of many
        index = 0
        for speech_segment in self.data_loader:

            if speech_segment.speaker_X[0] not in self.speakers:

                self.speakers.append(speech_segment.speaker_X[0])
                self.speaker_to_idx[speech_segment.speaker_X[0]] = index
                self.genders.append('female' if speech_segment.gender_Y_idx[0] == 0 else 'male')
                index += 1

            if speech_segment.speaker_Y[0] not in self.speakers:
                self.speakers.append(speech_segment.speaker_Y[0])
                self.speaker_to_idx[speech_segment.speaker_Y[0]] = index
                self.genders.append('female' if speech_segment.gender_Y_idx[0] == 0 else 'male')
                index += 1

            x_idx = self.speaker_to_idx[speech_segment.speaker_X[0]]
            y_idx = self.speaker_to_idx[speech_segment.speaker_Y[0]]

            self.speech_segment_count[x_idx] += 1
            self.speaker_pair_segments_count[x_idx][y_idx] += 1
