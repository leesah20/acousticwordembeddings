__author__ = "Lisa van Staden"

import json
import os
import numpy as np
import pickle


def frames_to_segments(language, save_path):
    if os.getcwd() == "/home":
        root = "/home"
    else:
        root = "../.."

    with open(root + "/config/data_paths.json") as data_paths_file:
        path_dict = json.load(data_paths_file)
        with open(path_dict[f"{language}_utd_pair_paths"], 'rb') as paths_file:
            paths = pickle.load(paths_file)
        utd_data = np.load(root + path_dict[f"{language}_train_data"])
        pair_keys = [line.strip().split(" ") for line in open(path_dict[f"{language}_utd_pair_keys"])]

    segment_dict = {}
    for i, [x_key, y_key] in enumerate(pair_keys):
        x, y = zip(*paths[i])
        x = list(x)
        y = list(y)
        x.reverse()
        y.reverse()
        segment_dict[f"{x_key}.X"] = utd_data[x_key][x, :]
        segment_dict[f"{x_key}.Y"] = utd_data[y_key][y, :]
        np.savez(save_path, **segment_dict)

if __name__ == '__main__':
    frames_to_segments(language='xitsonga', save_path="features/aligned_segments/xitsonga_segments.npz")


