__author__ = "Lisa van Staden"

import random

from torch.utils import data

from data_loading.SpeakerDataset import SpeakerDataset


class AdaptiveSpeakerBucketSampler(data.Sampler):

    def __init__(self, data_source: SpeakerDataset, num_speakers, num_buckets=3, pairs=False, shuffle=True):
        super().__init__(data_source)
        self.data_source = data_source
        self.num_buckets = num_buckets
        self.num_speakers = num_speakers
        self.pairs = pairs
        self.buckets = []
        self.shuffle = shuffle
        self._allocate_buckets()

    def __iter__(self):
        if not self.shuffle:
            random.seed(0)
        buckets = sum([self.buckets[i].get_utterances() for i in range(self.num_buckets)], [])
        return iter(buckets)

    def __len__(self):
        return len(self.data_source)

    def _allocate_buckets(self):
        buckets = [[] for _ in range(self.num_buckets)]
        seq_lengths = [(i, self.data_source[i].get("X_length")) for i in range(len(self.data_source))]
        lengths = list(seq_len for (i, seq_len) in seq_lengths)
        lengths.sort()
        bucket_means = []
        if len(lengths) < self.num_buckets:
            print("Error: Too many buckets")
        for i in range(self.num_buckets):
            bucket_means.append(lengths[(i + 1) * int(len(lengths) / (self.num_buckets + 1))])

        for (i, seq_len) in seq_lengths:
            dists = [abs(seq_len - bm) for bm in bucket_means]
            index = dists.index(min(dists))
            buckets[index].append(i)

        for i in range(self.num_buckets):
            self.buckets.append(Bucket(buckets[i], self.data_source, self.num_speakers, self.pairs))

    def update_num_speakers(self, num_speakers):
        self.num_speakers = num_speakers
        for b in self.buckets:
            b.update_num_speakers(self.num_speakers)


class Bucket:
    def __init__(self, utt_idxs, data_source, num_speakers, pairs):
        self.utt_idxs = utt_idxs
        self.data_source = data_source
        self.num_speakers = num_speakers
        self.speaker_utts = {}
        self.utts_count = []
        self.speaker_idxs = []
        self.pairs = pairs
        self.pair_idxs = [[]]
        self.pair_count = []
        self._allocate_speakers()

    def _allocate_speakers(self):
        print("allocating speaker utts in bucket", len(self.utt_idxs))
        for utt_idx in self.utt_idxs:
            speaker_idx = self.data_source[utt_idx].get("speaker_X_idx")
            if speaker_idx not in self.speaker_utts:
                self.speaker_utts[speaker_idx] = []
            self.speaker_utts[speaker_idx].append(utt_idx)
        for k in self.speaker_utts:
            self.utts_count.append(len(self.speaker_utts[k]))
            self.speaker_idxs.append(k)
        sorted_idxs = sorted(range(len(self.utts_count)), key=lambda i: self.utts_count[i], reverse=True)
        self.utts_count = [self.utts_count[i] for i in sorted_idxs]
        self.speaker_idxs = [self.speaker_idxs[i] for i in sorted_idxs]

        self.pair_idxs = [[[] for _ in range(self.num_speakers)] for _ in range(self.num_speakers)]
        self.pair_count = [0 for _ in range(self.num_speakers)]
        if self.pairs:
            for i in range(len(self.speaker_idxs)):
                for utt_idx in self.speaker_utts[self.speaker_idxs[i]]:
                    speaker_y_idx = self.data_source[utt_idx].get("speaker_Y_idx")
                    if speaker_y_idx not in self.speaker_idxs:
                        continue
                    self.pair_idxs[i][self.speaker_idxs.index(speaker_y_idx)].append(utt_idx)
            for i in range(len(self.speaker_idxs)):
                self.pair_count[i] = min([sum([len(self.pair_idxs[k][j]) for j in range(i + 1)], 0) for k in range(i + 1)])


            self.num_speakers = min(self.num_speakers, len(self.speaker_idxs))

    def get_utterances(self):
        if not self.pairs:
            batch_speakers = self.speaker_idxs[:self.num_speakers]
            num_utts = max(self.utts_count[self.num_speakers - 1], 200)
            bucket_batch = sum(
                [random.sample(self.speaker_utts[batch_speakers[i]], num_utts) if self.utts_count[i] >= num_utts
                 else random.sample(self.speaker_utts[batch_speakers[i]], self.utts_count[i])
                 for i in range(len(batch_speakers))], [])

        else:
            batch_speakers = [sum(speaker_i[:self.num_speakers], []) for speaker_i in
                              self.pair_idxs[:self.num_speakers]]
            num_utts = max(self.pair_count[self.num_speakers - 1], 1000)
            bucket_batch = sum(
                [random.sample(batch_speakers[i], num_utts) if self.pair_count[i] >= num_utts
                 else random.sample(batch_speakers[i], self.pair_count[i])
                 for i in range(len(batch_speakers))], [])

        random.shuffle(bucket_batch)
        return bucket_batch

    def update_num_speakers(self, num_speakers):
        self.num_speakers = min(num_speakers, len(self.speaker_idxs))
