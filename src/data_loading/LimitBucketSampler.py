__author__ = "Lisa van Staden"

from data_loading.BucketSampler import BucketSampler
from data_loading.SpeakerDataset import SpeakerDataset


class LimitBucketSampler(BucketSampler):
    def __init__(self, data_source: SpeakerDataset, num_buckets=4,
                 shuffle=True, num_speakers=-1, max_pairs_per_speaker=-1):
        print("Using speaker limiting bucket sampler")
        super().__init__(data_source, num_buckets, shuffle)
        self._num_speakers = num_speakers
        self._max_pairs_per_speaker = max_pairs_per_speaker

        self._speakers = {}

        for i in range(len(self.data_source)):
            speaker = self.data_source[i].get('speaker_X')
            if speaker not in self._speakers:
                self._speakers[speaker] = self.Speaker(speaker)
            self._speakers[speaker].add_i(i)
        self.flat_buckets = []

        if num_speakers > 0:
            self.set_speakers(self._num_speakers)

        if max_pairs_per_speaker > 0:
            self.remove_pairs()

        self._allocate_buckets()

    def set_speakers(self, num_speakers):
        # todo
        sorted_speakers = sorted(list(self._speakers.values()), reverse=True)
        self.flat_buckets = [i for j in range(num_speakers) for i in sorted_speakers[j].indices]

    def remove_pairs(self):
        sorted_speakers = sorted(list(self._speakers.values()), reverse=True)
        for speaker in sorted_speakers:
            if speaker.count <= self._max_pairs_per_speaker:
                break
            remove_idxs = speaker.indices[self._max_pairs_per_speaker:]
            self.flat_buckets = [i for i in self.flat_buckets if i not in remove_idxs]

    class Speaker(object):

        def __init__(self, speak_id):
            self.speak_id = speak_id
            self.indices = []
            self.count = 0

        def add_i(self, i):
            self.indices.append(i)
            self.count += 1

        def __lt__(self, other):
            return self.count < other.count
