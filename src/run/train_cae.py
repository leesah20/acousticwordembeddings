import json
import os
import sys

from modules.SpeakerPredictor import SpeakerPredictor
from training import AETrainer, CAETrainer
from training.SpeakerPredictorTrainer import SpeakerPredictorTrainer
from training import helpers

__author__ = "Lisa van Staden"


def main():
    config = None
    if os.getcwd() == "/home":
        root = "/home"
    else:
        root = "../.."
    with open(root + "/config/herman_model_config.json", "r") as json_file:
        config = json.load(json_file)
    if len(sys.argv) > 2:
        save_path = sys.argv[1]
        pretrained_path = sys.argv[2]
        cae_trainer = CAETrainer.CAETrainer(config, save_path=save_path, pretrained_ae_path=pretrained_path)
    elif len(sys.argv) == 1:
        cae_trainer = CAETrainer.CAETrainer(config, pretrained_ae_path="/saved_models/ae.pt")

    cae_trainer.build()
    cae_trainer.train()


if __name__ == '__main__':
    main()
