import argparse
import json

import torch

from training import AETrainer, CAETrainer, IncreasingSpeakerCAETrainer, IncreasingSpeakerAETrainer

__author__ = "Lisa van Staden"


class Arg(object):
    pass


def main(argv: Arg):

    config = json.load(argv.config_file)
    if argv.model_type == 'AE':

        if argv.slow_speaker_increase:
            trainer = IncreasingSpeakerAETrainer.IncreasingSpeakerAETrainer(config, save_path=argv.save_path,
                                                                              language=argv.language,
                                                                              speaker_adversarial=argv.speaker_adv,
                                                                              speaker_cond=argv.speaker_cond,
                                                                              gender_adversarial=argv.gender_adv,
                                                                              gender_cond=argv.gender_cond)
        else:
            trainer = AETrainer.AETrainer(config, save_path=argv.save_path, language=argv.language,
                                          speaker_adversarial=argv.speaker_adv, speaker_cond=argv.speaker_cond,
                                          gender_adversarial=argv.gender_adv, gender_cond=argv.gender_cond)
    elif argv.model_type == 'CAE':

        if argv.slow_speaker_increase:
            trainer = IncreasingSpeakerCAETrainer.IncreasingSpeakerCAETrainer(config, save_path=argv.save_path,
                                            pretrained_ae_path=argv.load_pretrained_ae, language=argv.language,
                                            speaker_adversarial=argv.speaker_adv, speaker_cond=argv.speaker_cond,
                                            gender_adversarial=argv.gender_adv, gender_cond=argv.gender_cond)
        else:
            trainer = CAETrainer.CAETrainer(config, save_path=argv.save_path,
                                        pretrained_ae_path=argv.load_pretrained_ae, language=argv.language,
                                        speaker_adversarial=argv.speaker_adv, speaker_cond=argv.speaker_cond,
                                        gender_adversarial=argv.gender_adv, gender_cond=argv.gender_cond)

    trainer.build()
    if argv.load_path:
        trainer.load_saved_model()
    elif argv.load_pretrained_ae is not None:
        print("loading pretrained ae")
        trainer.load_pretrained_ae()

    if argv.action in ['all', 'train']:
        # print("not training")
        # ignore
        trainer.train()

    if argv.action in ['all', 'evaluate']:
        torch.cuda.empty_cache()
        results = trainer.evaluate()

    if argv.action in ['extract']:
        torch.cuda.empty_cache()
        trainer.embeddings_to_file()



def check_args():
    argv = Arg()
    arg_parser = argparse.ArgumentParser(argument_default=None)
    arg_parser.add_argument('action', choices=['all', 'train', 'evaluate', 'extract'])
    arg_parser.add_argument('model_type', choices=['AE', 'CAE'])
    arg_parser.add_argument('config_file', type=argparse.FileType('r'))
    arg_parser.add_argument('save_path', type=str)
    arg_parser.add_argument('--load_path', action='store_true')
    arg_parser.add_argument('--load_pretrained_ae', type=str)
    arg_parser.add_argument('--language', default="english", choices=["english", "xitsonga", "hausa"])
    arg_parser.add_argument('--speaker_cond', action='store_true')
    arg_parser.add_argument('--speaker_adv', action='store_true')
    arg_parser.add_argument('--gender_cond', action='store_true')
    arg_parser.add_argument('--gender_adv', action='store_true')
    arg_parser.add_argument('--slow_speaker_increase', action='store_true')
    arg_parser.parse_args(namespace=argv)
    return argv


if __name__ == '__main__':
    arg = check_args()
    main(arg)
