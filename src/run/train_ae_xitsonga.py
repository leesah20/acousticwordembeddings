import json
import os
import torch
import sys

from modules.SpeakerPredictor import SpeakerPredictor
from training import AETrainer
from training.SpeakerPredictorTrainer import SpeakerPredictorTrainer
from training import helpers

__author__ = "Lisa van Staden"


def main():
    torch.cuda.empty_cache()
    config = None
    if os.getcwd() == "/home":
        root = "/home"
    else:
        root = "../.."
    with open(root + "/config/herman_model_config.json", "r") as json_file:
        config = json.load(json_file)
    if len(sys.argv) > 1:
        save_path = sys.argv[1]
        print(save_path)
        ae_trainer = AETrainer.AETrainer(config, save_path=save_path, language="xitsonga")
    else:
        ae_trainer = AETrainer.AETrainer(config)
    ae_trainer.build()
    ae_trainer.train(loss_fn=helpers.custom_mae)


if __name__ == '__main__':
    main()