import json
import os
import torch
import sys

from modules.SpeakerPredictor import SpeakerPredictor
from training import AETrainer
from training.SpeakerPredictorTrainer import SpeakerPredictorTrainer
from training import helpers

__author__ = "Lisa van Staden"


def main():
    if os.getcwd() == "/home":
        root = "/home"
    else:
        root = "../.."
    with open(root + "/config/herman_model_config.json", "r") as json_file:
        config = json.load(json_file)
    if len(sys.argv) > 1:
        save_path = sys.argv[1]
        ae_trainer = AETrainer.AETrainer(config, save_path=save_path)
    else:
        ae_trainer = AETrainer.AETrainer(config)
    ae_trainer.build()
    ae_trainer.load_saved_model()
    ae_trainer.evaluate(data="test")
    ae_trainer.evaluate_speaker_predictability(data="test")


if __name__ == '__main__':
    main()
