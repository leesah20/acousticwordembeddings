import json
import os
import torch
import sys

from modules.SpeakerPredictor import SpeakerPredictor
from training import AETrainer, CAETrainer
from training.SpeakerPredictorTrainer import SpeakerPredictorTrainer
from training import helpers

__author__ = "Lisa van Staden"


def main():
    torch.cuda.empty_cache()
    config = None
    if os.getcwd() == "/home":
        root = "/home"
    else:
        root = "../.."
    with open(root + "/config/herman_model_config.json", "r") as json_file:
        config = json.load(json_file)
    if len(sys.argv) > 1:
        save_path = sys.argv[1]
        ae_trainer = CAETrainer.CAETrainer(config, save_path=save_path, gender_cond=True, speaker_cond=True)
    else:
        ae_trainer = AETrainer.AETrainer(config, speaker_cond=True)
    ae_trainer.build()
    ae_trainer.train(loss_fn=helpers.custom_mse)


if __name__ == '__main__':
    main()
