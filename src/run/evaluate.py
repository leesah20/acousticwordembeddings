import json
import os
import sys

from training import AETrainer, CAETrainer

__author__ = "Lisa van Staden"


def main():
    if os.getcwd() == "/home":
        root = "/home"
    else:
        root = "../.."
    with open(root + "/config/herman_model_config.json", "r") as json_file:
        config = json.load(json_file)
        save_path = sys.argv[1]
        language = sys.argv[2]
        model_type = sys.argv[3]

        if model_type == "AE":
            ae_trainer = AETrainer.AETrainer(config, save_path=save_path, language=language)
        else:
            ae_trainer = CAETrainer.CAETrainer(config, save_path=save_path, language=language,
                                               speaker_cond=True, gender_cond=True)

    ae_trainer.build()
    ae_trainer.load_saved_model()
    ae_trainer.evaluate(data="test")
    ae_trainer.evaluate_speaker_predictability(data="test")


if __name__ == '__main__':
    main()
