import os
import sys

import numpy as np

__author__ = "Lisa van Staden"


def extract_test(print_latex_string=True, directory=".", predictability=False, result_only=False):
    all_results = []
    all_results_str = []
    print(directory)
    for out_file in os.listdir(directory):
        if out_file.endswith(".out"):
            print(out_file)
            testing = False
            norm = False
            gender = False
            gp = []
            sp = []
            prec_list = []
            prec_norm_list = []
            results_dict = {
                "sp": -1,
                "gp": -1,
                "ap": -1,
                "ap_norm": -1
            }
            with open("{0}/{1}".format(directory, out_file)) as f:
                for line in f:
                    if "Testing" in line:
                        testing = True
                        continue
                    if testing and "Precision:" in line:

                        if not norm:
                            prec_list.append(float(line.split(" ")[2]))
                        else:
                            prec_norm_list.append(float(line.split(" ")[3]))
                        norm = not norm
                    if predictability and testing and "Best" in line:
                        if not gender:
                            sp.append(float(line.split(' ')[3]))
                        else:
                            gp.append(float(line.split(' ')[3]))
                        gender = not gender

                prec_list = np.array(prec_list[:3])
                prec_norm_list = np.array(prec_norm_list[:3])
                if predictability:
                    sp = np.array(sp[:3])
                    gp = np.array(gp[:3])

                    result_str = (" ${0} \\pm {1} $ & ${2} \\pm {3} $ & $ {4} \\pm {5} $".format(
                        round(prec_norm_list.mean() * 100, 2),
                        round(prec_norm_list.std() * 100, 2),
                        round(sp.mean(), 2), round(sp.std(), 2),
                        round(gp.mean(), 2), round(gp.std(), 2)))
                else:
                    result_str = (" ${0} \\pm {1} $".format(
                       # round(prec_list.mean() * 100, 2),
                       # round(prec_list.std() * 100, 2),
                        round(prec_norm_list.mean() * 100, 2),
                        round(prec_norm_list.std() * 100, 2)))
                label_dict = {"ae": " - ",
                              "cae": " - ",
                              "speaker_cond": " - ",
                              "gender_cond": " - ",
                              "speaker_adv": " - ",
                              "gender_adv": " - "
                              }
                speaker = False
                gender = False
                for s in out_file.split('.')[0].split('_'):
                    if s == "ae":
                        label_dict["ae"] = "$*$"
                        results_dict["type"] = "ae"
                    elif s == "cae":
                        label_dict["cae"] = "$*$"
                        results_dict["type"] = "cae"
                    elif s == "speak":
                        speaker = True
                    elif s == "gend":
                        gender = True
                    elif s == "cond":
                        if speaker:
                            label_dict["speaker_cond"] = "$*$"
                            speaker = False
                        if gender:
                            label_dict["gender_cond"] = "$*$"
                            gender = False
                    elif s == "adv":
                        if speaker:
                            label_dict["speaker_adv"] = "$*$"
                            speaker = False
                        if gender:
                            label_dict["gender_adv"] = "$*$"
                            gender = False

                label_str = ""

                results_dict["ap"] = 0 if len(prec_list) == 0 else prec_list.mean() * 100
                results_dict["ap_norm"] = 0 if len(prec_list) == 0 else prec_norm_list.mean() * 100
                if predictability:
                    results_dict["sp"] = 0 if len(prec_list) == 0 else sp.mean()
                    results_dict["gp"] = 0 if len(prec_list) == 0 else gp.mean()

                for key in label_dict:
                    label_str += label_dict[key] + "&"

                if print_latex_string:
                    all_results_str.append(f"{label_str} {result_str} \\\\")

                all_results.append(results_dict)
    if print_latex_string:
        for r in all_results_str:
            print(r)
    elif result_only:
        for r in all_results:
            print(r)
    return all_results

if __name__ == '__main__':
    extract_test(print_latex_string=True, directory=sys.argv[1])
