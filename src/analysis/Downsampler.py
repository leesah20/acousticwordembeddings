__author__ = "Lisa van Staden"

import numpy as np
from scipy import signal
from analysis import evaluate_embeddings_from_npz


class Downsampler:

    def __init__(self, input_npz, output_npz, suffix=None):
        self.input_npz = input_npz
        self.output_npz = output_npz
        self.suffix = suffix

    def sample_down(self, n_samples=10, d_frame=256, flatten_order="C"):
        input_dict = np.load(self.input_npz)
        output_dict = {}

        for key in input_dict:
            if self.suffix is not None and key.split(".")[1] != self.suffix:
                continue
            y = input_dict[key][:, :d_frame].T
            output_dict[key] = signal.resample(y, n_samples, axis=1).flatten(flatten_order)

        np.savez_compressed(self.output_npz, **output_dict)

    def evaluate(self):
        evaluate_embeddings_from_npz.evaluate(self.output_npz)

if __name__ == '__main__':

    for i in range(1, 4):
        downsampler = Downsampler(input_npz=f'/home/features/capc_features/xitsonga/capc_feats_test_{i}.npz',
                                  output_npz='downsampled.npz')
        downsampler.sample_down()
        downsampler.evaluate()