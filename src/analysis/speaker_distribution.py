__author__ = "Lisa van Staden"

import colorsys
import itertools
import sys
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pylab
import numpy as np
from data_loading.SpeakerDataLoader import SpeakerDataLoader
from data_loading.SpeakerInfoExtractor import SpeakerInfoExtractor


def get_speaker_dist(language='english', data_type='training'):

    speak_extractor = SpeakerInfoExtractor(language, data_type)

    speakers = speak_extractor.speakers
    genders = speak_extractor.genders
    speech_count = speak_extractor.speech_segment_count.detach().cpu().numpy()[:len(speakers)]
    sorted_indexes = np.argsort(speech_count).tolist()
    colors = ['blue' if genders[i] == 'male' else 'red' for i in range(len(speakers))]
    sorted_colors = [colors[i] for i in sorted_indexes]
    sorted_speakers = [speakers[i] for i in sorted_indexes]
    sorted_speech_count = [speech_count[i] for i in sorted_indexes]
    female_patch = mpatches.Patch(color='red', label='female')
    male_patch = mpatches.Patch(color='blue', label='male')
    fig = plt.figure(figsize=(7 * len(speakers)/12, 7))
    plt.rc('font', size=10)
    plt.legend(handles=[female_patch, male_patch])
    plt.bar(sorted_speakers, sorted_speech_count, color=sorted_colors)
    plt.xlabel('Speakers')
    plt.ylabel('Number of speech segments')
    plt.title(f'Number of speech segments per speaker from the {language} {data_type} data')
    plt.savefig(f'speaker_distribution_{language}_{data_type}.pdf')
    plt.tight_layout()
    plt.close()

    speak_pairs_matrix = speak_extractor.speaker_pair_segments_count.detach().cpu().numpy()
    fig = plt.figure(figsize=(7 * len(speakers) / 12 , 7 * len(speakers) / 12))
    plt.rc('font', size=10)
    plt.tight_layout(pad=2)
    plt.imshow(speak_pairs_matrix, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title(f'Pair count from the {language} {data_type} data')
    plt.ylabel('Speakers Y')
    plt.xlabel('Speakers X')
    plt.gca().invert_yaxis()
    plt.xticks(np.arange(len(speakers)), speakers)
    plt.yticks(np.arange(len(speakers)), speakers)

    white_threshold = np.max(speak_pairs_matrix) / 2
    for i, j in itertools.product(range(len(speakers)), range(len(speakers))):
        plt.text(j, i, int(speak_pairs_matrix[i, j]),
                 horizontalalignment="center",
                 color='white' if speak_pairs_matrix[i, j] > white_threshold else 'black')
    plt.savefig(f'speaker_pair_matrix_{language}_{data_type}.pdf')
    plt.close()


if __name__ == '__main__':
    for language in ['english', 'xitsonga', 'hausa']:
        print(language)
        for dt in ['training', 'validation', 'test']:
            print(dt)
            get_speaker_dist(language, dt)
