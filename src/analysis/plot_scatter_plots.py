import sys

from analysis import extract_results
import matplotlib.pyplot as plt
import numpy as np

__author__ = "Lisa van Staden"


def scatter_plots_by_dir(results_dir="."):
    results = extract_results.extract_test(False, directory=results_dir)
    ae_results = []
    for result in results:
        if result["type"] == "ae":
            if result["ap"] == 0:
                continue
            ae_results.append(result)

    aps = np.array([result["ap_norm"] for result in ae_results])
    sps = np.array([result["sp"] for result in ae_results])
    gps = np.array([result["gp"] for result in ae_results])

    print(aps)

    # ap vs sp
    plt.scatter(sps, aps)
    # p_fn = np.poly1d(np.polyfit(sps, aps, 1))
    #  plt.plot(sps, p_fn(sps))
    plt.ylabel("Average Precision")
    plt.xlabel("Speaker Predictability")
    plt.savefig("ae_ap_vs_sp.png")

    plt.close()

    # ap vs gp
    plt.scatter(gps, aps)
    # plt.plot(np.unique(gps), np.poly1d(np.polyfit(gps, aps, 1))(np.unique(gps)))
    plt.ylabel("Average Precision")
    plt.xlabel("Gender Predictability")
    plt.savefig("ae_ap_vs_gp.png")

    plt.close()

    cae_results = []
    for result in results:
        if result["type"] == "cae":
            if result["ap"] == 0:
                continue
            cae_results.append(result)

    aps = np.array([result["ap_norm"] for result in cae_results])
    sps = np.array([result["sp"] for result in cae_results])
    gps = np.array([result["gp"] for result in cae_results])

    # ap vs sp
    plt.scatter(sps, aps)
    # plt.plot(np.unique(sps), np.poly1d(np.polyfit(sps, aps, 1))(np.unique(sps)))
    plt.ylabel("Average Precision")
    plt.xlabel("Speaker Predictability")
    plt.savefig("cae_ap_vs_sp.png")

    plt.close()

    # ap vs gp
    plt.scatter(gps, aps)
    # plt.plot(np.unique(gps), np.poly1d(np.polyfit(gps, aps, 1))(np.unique(gps)))
    plt.ylabel("Average Precision")
    plt.xlabel("Gender Predictability")
    plt.savefig("cae_ap_vs_gp.png")

    plt.close()


def scatter_plots_from_list(aps, sps, gps, save_path, ap_vars=None, sp_vars=None, gp_vars=None):
    markers = ['o', 's', '^', 'd']
    group_idxs = [0, 1, 8, 10, 12]
    error_sp = plt.errorbar(sps, aps, xerr=sp_vars, yerr=ap_vars, c='blue', fmt='.', alpha=0.2)
    error_gp = plt.errorbar(gps, aps, xerr=gp_vars, yerr=ap_vars, c='green', fmt='.', alpha=0.2)
    types = []
    for i in range(len(markers)):
        types.append(plt.scatter(sps[group_idxs[i]: group_idxs[i + 1]], aps[group_idxs[i]: group_idxs[i + 1]], c='blue',
                                 marker=markers[i]))
        plt.scatter(gps[group_idxs[i]: group_idxs[i + 1]], aps[group_idxs[i]: group_idxs[i + 1]], c='green',
                    marker=markers[i])
    legned_pred = plt.legend(
        [plt.plot([], ls='', marker='+', color='blue')[0], plt.plot([], ls='', marker='+', color='green')[0]]
        , ["speaker", "gender"], title="Predictability", bbox_to_anchor=(1.01, 1), loc='upper left')
    ax = plt.gca().add_artist(legned_pred)
    legend_type = plt.legend(types, ["none", "conditioned", "adversarial", "combined"], title="Normalise method",
                             bbox_to_anchor=(1.01, 0.8), loc='upper left')
    for i in range(len(markers)):
        legend_type.legendHandles[i].set_color('black')
    ax = plt.gca().add_artist(legend_type)
    plt.ylabel("Average Precision %")
    plt.xlabel("Predictability %")
    m_sp, b_sp = np.polyfit(sps, aps, 1)
    coef = np.corrcoef(sps, aps)[0][1]
    plt.plot(sps, m_sp * sps + b_sp, c='gray')
    plt.text(np.min(sps) - 3, np.min(sps) * m_sp + b_sp - 0.15, fr'$r = {np.round(coef, 2)}$',)
    m_gp, b_gp = np.polyfit(gps, aps, 1)
    plt.plot(gps, m_gp*gps + b_gp, c='gray')
    coef = np.corrcoef(gps, aps)[0][1]
    plt.text(np.min(gps) - 4, np.min(sps) * m_sp + b_sp - 0.15, fr'$r = {np.round(coef, 2)}$')
    plt.savefig(save_path, bbox_extra_artists=(legned_pred, legend_type), bbox_inches='tight')
    plt.close()


if __name__ == '__main__':
    # ae
    aps = np.array([26.89, 27.83, 27.91, 27.54, 27.35, 26.80, 27.51, 27.15, 27.30, 27.03, 27.60, 27.56])
    ap_vars = [0.39, 0.22, 0.34, 0.31, 0.39, 0.28, 0.51, 0.27, 0.44, 0.10, 0.45, 0.61]
    sps = np.array([65.27, 52.10, 52.10, 60.39, 53.87, 51.13, 55.58, 59.9, 58.01, 60.33, 58.56, 57.04])
    sp_vars = [4.30, 1.19, 2.35, 2.77, 0.87, 3.05, 1.28, 1.99, 2.99, 4.30, 1.72, 4.27]
    gps = np.array([84.28, 77.15, 75.93, 80.07, 77.09, 75.81, 78.0, 81.47, 78.79, 83.55, 79.22, 77.7])
    gp_vars = [1.70, 0.68, 1.84, 1.42, 0.31, 1.64, 0.38, 1.21, 0.90, 0.91, 1.35, 1.04]

    scatter_plots_from_list(aps, sps, gps, save_path="/home/figs/ae_scatter.pdf", ap_vars=ap_vars, sp_vars=sp_vars,
                            gp_vars=gp_vars)

    '''aps = np.array([32.05, 34.45, 33.70, 33.06, 35.22, 32.28, 33.76, 33.36, 32.35, 32.11, 34.17, 33.49])
    ap_vars = [0.37, 0.73, 0.91, 0.20, 0.75, 0.63, 0.84, 0.40, 0.19, 0.09, 0.20, 1.12]
    sps = np.array([66.00, 53.32, 53.50, 59.6, 52.71, 51.92, 57.59, 61.79, 61.43, 63.50, 46.16, 56.55])
    sp_vars = [4.93, 1.35, 2.93, 2.74, 1.21, 3.33, 2.20, 1.03, 5.19, 3.13, 1.41, 3.96]
    gps = np.array([82.75, 77.03, 81.47, 81.05, 78.24, 78.85, 79.34, 83.79, 83.49, 85.92, 79.53, 80.99])
    gp_vars = [2.61, 0.60, 1.12, 2.31, 0.91, 2.53, 0.79, 0.86, 2.14, 1.51, 2.11, 1.79]

    scatter_plots_from_list(aps, sps, gps, save_path="/home/figs/cae_scatter.pdf", ap_vars=ap_vars, sp_vars=sp_vars,
                            gp_vars=gp_vars)'''
