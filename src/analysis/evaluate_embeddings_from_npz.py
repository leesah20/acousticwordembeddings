__author__ = "Lisa van Staden"

import sys
import numpy as np

from training import helpers


def evaluate(npz_file):
    npz = np.load(npz_file)
    utt_keys = [key for key in sorted(npz)]
    labels = [utt_key.split("_")[0] for utt_key in utt_keys]
    # print(labels)
    z = np.array([npz[k] for k in utt_keys])

    ap, prb = helpers.calculate_ap_and_prb(z, labels)
    print(ap)

if __name__ == '__main__':
    evaluate(sys.argv[1])