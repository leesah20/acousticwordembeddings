import sys

from analysis import extract_results
import matplotlib.pyplot as plt
import numpy as np

__author__ = "Lisa van Staden"

def autolabel(ax, rects):
    """Attach a text label above each bar in *rects*, displaying its height.
    from: https://matplotlib.org/3.1.1/gallery/lines_bars_and_markers/barchart.html"""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

def histograms(results_dir="."):
    #results = extract_results.extract_test(False, directory=results_dir)
    results_eng = {
        "AE": 25.19,
        "AE-Speak&Gend-Cond-Gend-Adv": 25.53,
        "AE-Gend-Adv": 25.38,
        "CAE": 30.18,
        "CAE-Speak_cond-Speak-Adv": 30.49,
        "CAE-Gend-Cond-Gend-Adv": 29.72
    }

    results_xit = {
        "AE": 11.65,
        "AE-Speak&Gend-Cond-Gend-Adv": 12.78,
        "AE-Gend-Adv": 11.22,
        "CAE": 22.52,
        "CAE-Speak_cond-Speak-Adv": 28.98,
        "CAE-Gend-Cond-Gend-Adv": 22.72
    }

    x = np.arange(len(results_eng.keys()))
    width = 0.35

    eng_values = np.array([v for v in results_eng.values()])
    xit_values = np.array([v for v in results_xit.values()])

    fig, ax = plt.subplots()
    bars1 = ax.bar(x - width / 2, eng_values, width, label="English")
    bars2 = ax.bar(x + width / 2, xit_values, width, label="Xitsonga")
    ax.set_ylabel('Average Precision (%)')
    ax.set_xlabel('Model Type')
    ax.set_xticks(x)
    ax.set_xticklabels(["AE-Baseline", "AE-Top-1", "AE-Top-2", "CAE-Baseline", "CAE-Top-1", "CAE-Top-2"])
    autolabel(ax, bars1)
    autolabel(ax, bars2)
    ax.legend()
    fig.tight_layout()
    plt.savefig("histgram.pdf")



if __name__ == '__main__':
    histograms(sys.argv[1])
