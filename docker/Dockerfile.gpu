# Docker image for Pytorch and HTK
# Lisa van Staden

# Requires that `HTK-samples-3.4.1.tar.gz` be in the same directory as where
# the image is created from. After registering, this tarball can be obtained
# from http://htk.eng.cam.ac.uk/download.shtml.

# Parent image
FROM ufoym/deepo:pytorch-py36



# Standard tools (Python 3.6)
RUN apt-get update && \
    python -m pip --no-cache-dir install --upgrade \
        setuptools \
        pip \
        && \
    python -m pip --no-cache-dir install --upgrade \
        ipython \
        nose \
        jupyter \
        librosa

# Install unbuffer command
ENV TZ=Africa/Johannesburg
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update  -y && \
    apt-get install -y expect

# Install sox
RUN apt-get install -y sox


# Install HTK dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    g++ \
    gcc-multilib \
    libc6-i386 \
    libc6-dev-i386

# Install HTK
ADD HTK-3.4.1.tar.gz /src/
WORKDIR /src/htk/
RUN ./configure --disable-hslab && \
    sed -i 's/\ \ \ \ \ \ \ \ if/\tif/' HLMTools/Makefile && \
    make all && \
    make install

# Install GitHub repositories
RUN git clone https://github.com/kamperh/speech_dtw.git /src/speech_dtw/
WORKDIR /src/speech_dtw
RUN make

# Working directory
WORKDIR /home